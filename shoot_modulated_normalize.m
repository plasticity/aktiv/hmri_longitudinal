addpath('lib')
addpath(fullfile('lib', 'bids'))

setup_paths
warning('off', 'MATLAB:MKDIR:DirectoryExists');
log_file = start_log(fullfile(deriv_path, 'logs'), mfilename);
spm('defaults','FMRI');

subjects = get_subjects(nifti_path, 'full');
clear map1 map2 def final_temp files_temp files_map files_def
for i=1:length(subjects)
    subject = cell2mat(subjects(i));
    sessions = get_sessions(nifti_path, subject);
        if isfile(spm_select('FPList', fullfile(deriv_path, 'MPM_noreorient', subject), '^mwc1.*\MTsat_Template\.nii$'))
            fprintf('Modulated normalised already exists, skipping %s\n', subject);
        elseif ~isfile(spm_select('FPList', fullfile(deriv_path, 'MPM_noreorient', subject), '^median.*\MTsat\.nii$'))
            fprintf('Median not available, skipping %s\n', subject);
        else
            tmpid = get_tmpid_s();
            in_dir = fullfile(deriv_path, 'MPM_noreorient', subject);
            out_dir = fullfile(deriv_path, 'MPM_noreorient', 'mni');
            mkdir(fullfile(out_dir, 'batches'));
            mkdir(fullfile(out_dir, 'logs'));
            batchfile = fullfile(out_dir, 'batches', ['shoot3-%j-', tmpid, '.mat']);
            batchlog = fullfile(out_dir, 'logs', ['shoot3-%j-', tmpid, '.log']);
            batchcmd = sprintf('%s %s batch %s', compiled_hmri, mcr_path, batchfile);
            
            if isfile(spm_select('FPList', in_dir, '^c1median.*\MTsat\.nii$'))
                map1(i,:) = spm_select('FPList', in_dir, '^c1median.*\MTsat\.nii$');
                map2(i,:) = spm_select('FPList', in_dir, '^c2median.*\MTsat\.nii$');
            
                def(i,:) = spm_select('FPList', in_dir, '^y_rc1median.*_MTsat_Template\.nii$');
            else
                fprintf('Segmentation not available for subject %s\n', subject);
            end
        end
end % subject

final_temp = spm_select('FPList', fullfile(deriv_path, 'MPM_noreorient', 'shoot'), '^Template_4\.nii$');

files_temp = cellstr(final_temp);
files_map = { cellstr(map1), cellstr(map2) };
files_def = cellstr(def);

% remove empty rows if subject or time point not available
files_map{1}=files_map{1}(~cellfun('isempty',files_map{1}));
files_map{2}=files_map{2}(~cellfun('isempty',files_map{2}));

files_def=files_def(~cellfun('isempty',files_def));

fprintf('Creating batch for shoot modulated normalize ...\n');
clear matlabbatch
matlabbatch{1}.spm.tools.shoot.norm.template = files_temp;
matlabbatch{1}.spm.tools.shoot.norm.data.subjs.deformations = files_def;
matlabbatch{1}.spm.tools.shoot.norm.data.subjs.images = files_map;
matlabbatch{1}.spm.tools.shoot.norm.vox = [NaN NaN NaN];
matlabbatch{1}.spm.tools.shoot.norm.bb = [NaN NaN NaN
    NaN NaN NaN];
matlabbatch{1}.spm.tools.shoot.norm.preserve = 1;
matlabbatch{1}.spm.tools.shoot.norm.fwhm = 0;

save(batchfile, 'matlabbatch');

fprintf('Submitting\n');
system(sprintf('sbatch -D. -c 1 -J %s -o %s --wrap "%s"', ...
    'shoot3', batchlog, batchcmd));

stop_log(log_file);
