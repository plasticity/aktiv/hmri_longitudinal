addpath('lib')
addpath(fullfile('lib', 'aktiv'))

setup_paths
warning('off', 'MATLAB:MKDIR:DirectoryExists');
log_file = start_log(fullfile(deriv_path, 'logs'), mfilename);
spm('defaults','FMRI');

subjects = get_subjects(dicom_path, 'full');
for i=1:length(subjects)
    legacy_subject = cell2mat(subjects(i));
    [subject, session] = get_sessions(legacy_subject);
    out_dir = fullfile(base_path, 'rawdata', subject, session);
    mkdir(fullfile(out_dir, 'batches'));
    mkdir(fullfile(out_dir, 'logs'));
    tmpid = get_tmpid_s();
    batchfile = fullfile(out_dir, 'batches', ['dcms2nifti-', tmpid, '.mat']);
    batchlog = fullfile(out_dir, 'logs', ['dcms2nifti-%j-', tmpid, '.log']);
    batchcmd = sprintf('%s %s batch %s', compiled_hmri, mcr_path, batchfile);

    if length(dir(fullfile(out_dir, '**', '*.nii'))) ~= 0
        fprintf('Found niftis, skipping conversion for %s ...\n', legacy_subject);
    else
        fprintf('Creating batch for %s ...\n', legacy_subject);

        files=cell(1,9);
        mkdir(fullfile(out_dir, 'B1mapping'));
        mkdir(fullfile(out_dir, 'MPrage_3Ddist'));
        mkdir(fullfile(out_dir, 'MT', 'magnitude'));
        mkdir(fullfile(out_dir, 'MT', 'phase'));
        mkdir(fullfile(out_dir, 'PD', 'magnitude'));
        mkdir(fullfile(out_dir, 'PD', 'phase'));
        mkdir(fullfile(out_dir, 'T1', 'magnitude'));
        mkdir(fullfile(out_dir, 'T1', 'phase'));
        mkdir(fullfile(out_dir, 'B0', 'magnitude'));
        mkdir(fullfile(out_dir, 'B0', 'phase'));

        files{1}=spm_select('FPList', fullfile(dicom_path, 'MPM', 'B1mapping', legacy_subject), '^AKTIV.*\.IMA');
        files{2}=spm_select('FPList', fullfile(dicom_path, 'MPrage_3Ddist', legacy_subject), '^AKTIV.*\.IMA');


        subdirs=dir(fullfile(dicom_path, 'MPM', 'MT', legacy_subject));

        if length(subdirs)==4
            sequ11=subdirs(3).name;
            sequ12=subdirs(4).name;
            dicoms11=dir(fullfile(dicom_path, 'MPM', 'MT', legacy_subject, sequ11));
            dicoms12=dir(fullfile(dicom_path, 'MPM', 'MT', legacy_subject, sequ12));
            dcinfo11=dicominfo(fullfile(dicoms11(3).folder, dicoms11(3).name));
            dcinfo12=dicominfo(fullfile(dicoms12(3).folder, dicoms12(3).name));
            if strcmp(dcinfo11.ImageType, 'ORIGINAL\PRIMARY\M\ND')
                assert(strcmp(dcinfo12.ImageType, 'ORIGINAL\PRIMARY\P\ND'), 'Expected to find Phase image data in dicom');
                a=3; b=4;
            elseif strcmp(dcinfo12.ImageType, 'ORIGINAL\PRIMARY\M\ND')
                assert(strcmp(dcinfo11.ImageType, 'ORIGINAL\PRIMARY\P\ND'), 'Expected to find Phase image data in dicom');
                a=4; b=3;
            else
                error('No magnitude data found');
            end

            files{a}=spm_select('FPList', fullfile(dicom_path, 'MPM', 'MT', legacy_subject, sequ11), '^AKTIV.*\.IMA');
            files{b}=spm_select('FPList', fullfile(dicom_path, 'MPM' ,'MT', legacy_subject, sequ12), '^AKTIV.*\.IMA');
        else
            fprintf('Warning: Expected two subdirectories in %s for Subject %s\n', 'MT', legacy_subject);
        end


        subdirs=dir(fullfile(dicom_path, 'MPM', 'PD', legacy_subject));

        if length(subdirs)==4
            sequ21=subdirs(3).name;
            sequ22=subdirs(4).name;
            dicoms21=dir(fullfile(dicom_path, 'MPM', 'PD', legacy_subject, sequ21));
            dicoms22=dir(fullfile(dicom_path, 'MPM', 'PD', legacy_subject, sequ22));
            dcinfo21=dicominfo(fullfile(dicoms21(3).folder, dicoms21(3).name));
            dcinfo22=dicominfo(fullfile(dicoms22(3).folder, dicoms22(3).name));
            if strcmp(dcinfo21.ImageType, 'ORIGINAL\PRIMARY\M\ND')
                assert(strcmp(dcinfo22.ImageType, 'ORIGINAL\PRIMARY\P\ND'), 'Expected to find Phase image data in dicom');
                a=5; b=6;
            elseif strcmp(dcinfo22.ImageType, 'ORIGINAL\PRIMARY\M\ND')
                assert(strcmp(dcinfo21.ImageType, 'ORIGINAL\PRIMARY\P\ND'), 'Expected to find Phase image data in dicom');
                a=6; b=5;
            else
                error('No magnitude data found');
            end

            files{a}=spm_select('FPList', fullfile(dicom_path, 'MPM', 'PD', legacy_subject, sequ21), '^AKTIV.*\.IMA');
            files{b}=spm_select('FPList', fullfile(dicom_path, 'MPM', 'PD', legacy_subject, sequ22), '^AKTIV.*\.IMA');
        else
            fprintf('Warning: Expected two subdirectories in %s for Subject %s\n', 'PD', legacy_subject);
        end

        subdirs=dir(fullfile(dicom_path, 'MPM', 'T1', legacy_subject));

        if length(subdirs)==4
            sequ31=subdirs(3).name;
            sequ32=subdirs(4).name;
            dicoms31=dir(fullfile(dicom_path, 'MPM', 'T1', legacy_subject, sequ31));
            dicoms32=dir(fullfile(dicom_path, 'MPM', 'T1', legacy_subject, sequ32));
            dcinfo31=dicominfo(fullfile(dicoms31(3).folder, dicoms31(3).name));
            dcinfo32=dicominfo(fullfile(dicoms32(3).folder, dicoms32(3).name));
            if strcmp(dcinfo31.ImageType, 'ORIGINAL\PRIMARY\M\ND')
                assert(strcmp(dcinfo32.ImageType, 'ORIGINAL\PRIMARY\P\ND'), 'Error: expected to find Phase image data in dicom');
                a=7; b=8;
            elseif strcmp(dcinfo32.ImageType, 'ORIGINAL\PRIMARY\M\ND')
                assert(strcmp(dcinfo31.ImageType, 'ORIGINAL\PRIMARY\P\ND'), 'Error: expected to find Phase image data in dicom');
                a=8; b=7;
            else
                error('No magnitude data found');
            end
            files{a}=spm_select('FPList', fullfile(dicom_path, 'MPM', 'T1', legacy_subject, sequ31), '^AKTIV.*\.IMA');
            files{b}=spm_select('FPList', fullfile(dicom_path, 'MPM', 'T1', legacy_subject, sequ32), '^AKTIV.*\.IMA');
        else
            fprintf('Warning: Expected two subdirectories in %s for Subject %s\n', 'T1', legacy_subject);
        end

        subdirs=dir(fullfile(dicom_path, 'GRE_Field', legacy_subject));

        if length(subdirs)==4
            sequ41=subdirs(3).name;
            sequ42=subdirs(4).name;
            dicoms41=dir(fullfile(dicom_path, 'GRE_Field', legacy_subject, sequ41));
            dicoms42=dir(fullfile(dicom_path, 'GRE_Field', legacy_subject, sequ42));
            dcinfo41=dicominfo(fullfile(dicoms41(3).folder, dicoms41(3).name));
            dcinfo42=dicominfo(fullfile(dicoms42(3).folder, dicoms42(3).name));
            if strcmp(dcinfo41.ImageType, 'ORIGINAL\PRIMARY\M\ND')
                assert(strcmp(dcinfo42.ImageType, 'ORIGINAL\PRIMARY\P\ND'), 'Error: expected to find Phase image data in dicom');
                a=9; b=10;
            elseif strcmp(dcinfo42.ImageType, 'ORIGINAL\PRIMARY\M\ND')
                assert(strcmp(dcinfo41.ImageType, 'ORIGINAL\PRIMARY\P\ND'), 'Error: expected to find Phase image data in dicom');
                a=10; b=9;
            else
                error('No magnitude data found');
            end

            files{a}=spm_select('FPList', fullfile(dicom_path, 'GRE_Field', legacy_subject, sequ41), '^AKTIV.*\.IMA');
            files{b}=spm_select('FPList', fullfile(dicom_path, 'GRE_Field', legacy_subject, sequ42), '^AKTIV.*\.IMA');
        else
            fprintf('Warning: Expected two subdirectories in %s for Subject %s\n', 'GRE_Field', legacy_subject);
        end

        input=cell(10,1);
        for input_idx=1:10
            for file=1:size(files{input_idx},1)
                input{input_idx}{file,1}=files{input_idx}(file,1:size(files{input_idx},2));
            end
        end

        clear matlabbatch
        matlabbatch{1}.spm.tools.hmri.dicom.data = input{1};
        matlabbatch{1}.spm.tools.hmri.dicom.root = 'flat';
        matlabbatch{1}.spm.tools.hmri.dicom.outdir = {fullfile(out_dir, 'B1mapping')};
        matlabbatch{1}.spm.tools.hmri.dicom.protfilter = '.*';
        matlabbatch{1}.spm.tools.hmri.dicom.convopts.format = 'nii';
        matlabbatch{1}.spm.tools.hmri.dicom.convopts.metaopts.mformat = 'sep';
        matlabbatch{1}.spm.tools.hmri.dicom.convopts.icedims = 0;

        matlabbatch{2}.spm.tools.hmri.dicom.data = input{2};
        matlabbatch{2}.spm.tools.hmri.dicom.root = 'flat';
        matlabbatch{2}.spm.tools.hmri.dicom.outdir = {fullfile(out_dir, 'MPrage_3Ddist')};
        matlabbatch{2}.spm.tools.hmri.dicom.protfilter = '.*';
        matlabbatch{2}.spm.tools.hmri.dicom.convopts.format = 'nii';
        matlabbatch{2}.spm.tools.hmri.dicom.convopts.metaopts.mformat = 'sep';
        matlabbatch{2}.spm.tools.hmri.dicom.convopts.icedims = 0;

        matlabbatch{3}.spm.tools.hmri.dicom.data = input{3};
        matlabbatch{3}.spm.tools.hmri.dicom.root = 'flat';
        matlabbatch{3}.spm.tools.hmri.dicom.outdir = {fullfile(out_dir, 'MT', 'magnitude')};
        matlabbatch{3}.spm.tools.hmri.dicom.protfilter = '.*';
        matlabbatch{3}.spm.tools.hmri.dicom.convopts.format = 'nii';
        matlabbatch{3}.spm.tools.hmri.dicom.convopts.metaopts.mformat = 'sep';
        matlabbatch{3}.spm.tools.hmri.dicom.convopts.icedims = 0;

        matlabbatch{4}.spm.tools.hmri.dicom.data = input{4};
        matlabbatch{4}.spm.tools.hmri.dicom.root = 'flat';
        matlabbatch{4}.spm.tools.hmri.dicom.outdir = {fullfile(out_dir, 'MT', 'phase')};
        matlabbatch{4}.spm.tools.hmri.dicom.protfilter = '.*';
        matlabbatch{4}.spm.tools.hmri.dicom.convopts.format = 'nii';
        matlabbatch{4}.spm.tools.hmri.dicom.convopts.metaopts.mformat = 'sep';
        matlabbatch{4}.spm.tools.hmri.dicom.convopts.icedims = 0;

        matlabbatch{5}.spm.tools.hmri.dicom.data = input{5};
        matlabbatch{5}.spm.tools.hmri.dicom.root = 'flat';
        matlabbatch{5}.spm.tools.hmri.dicom.outdir = {fullfile(out_dir, 'PD', 'magnitude')};
        matlabbatch{5}.spm.tools.hmri.dicom.protfilter = '.*';
        matlabbatch{5}.spm.tools.hmri.dicom.convopts.format = 'nii';
        matlabbatch{5}.spm.tools.hmri.dicom.convopts.metaopts.mformat = 'sep';
        matlabbatch{5}.spm.tools.hmri.dicom.convopts.icedims = 0;

        matlabbatch{6}.spm.tools.hmri.dicom.data = input{6};
        matlabbatch{6}.spm.tools.hmri.dicom.root = 'flat';
        matlabbatch{6}.spm.tools.hmri.dicom.outdir = {fullfile(out_dir, 'PD', 'phase')};
        matlabbatch{6}.spm.tools.hmri.dicom.protfilter = '.*';
        matlabbatch{6}.spm.tools.hmri.dicom.convopts.format = 'nii';
        matlabbatch{6}.spm.tools.hmri.dicom.convopts.metaopts.mformat = 'sep';
        matlabbatch{6}.spm.tools.hmri.dicom.convopts.icedims = 0;

        matlabbatch{7}.spm.tools.hmri.dicom.data = input{7};
        matlabbatch{7}.spm.tools.hmri.dicom.root = 'flat';
        matlabbatch{7}.spm.tools.hmri.dicom.outdir = {fullfile(out_dir, 'T1', 'magnitude')};
        matlabbatch{7}.spm.tools.hmri.dicom.protfilter = '.*';
        matlabbatch{7}.spm.tools.hmri.dicom.convopts.format = 'nii';
        matlabbatch{7}.spm.tools.hmri.dicom.convopts.metaopts.mformat = 'sep';
        matlabbatch{7}.spm.tools.hmri.dicom.convopts.icedims = 0;

        matlabbatch{8}.spm.tools.hmri.dicom.data = input{8};
        matlabbatch{8}.spm.tools.hmri.dicom.root = 'flat';
        matlabbatch{8}.spm.tools.hmri.dicom.outdir = {fullfile(out_dir, 'T1', 'phase')};
        matlabbatch{8}.spm.tools.hmri.dicom.protfilter = '.*';
        matlabbatch{8}.spm.tools.hmri.dicom.convopts.format = 'nii';
        matlabbatch{8}.spm.tools.hmri.dicom.convopts.metaopts.mformat = 'sep';
        matlabbatch{8}.spm.tools.hmri.dicom.convopts.icedims = 0;

        matlabbatch{9}.spm.tools.hmri.dicom.data = input{9};
        matlabbatch{9}.spm.tools.hmri.dicom.root = 'flat';
        matlabbatch{9}.spm.tools.hmri.dicom.outdir = {fullfile(out_dir, 'B0', 'magnitude')};
        matlabbatch{9}.spm.tools.hmri.dicom.protfilter = '.*';
        matlabbatch{9}.spm.tools.hmri.dicom.convopts.format = 'nii';
        matlabbatch{9}.spm.tools.hmri.dicom.convopts.metaopts.mformat = 'sep';
        matlabbatch{9}.spm.tools.hmri.dicom.convopts.icedims = 0;

        matlabbatch{10}.spm.tools.hmri.dicom.data = input{10};
        matlabbatch{10}.spm.tools.hmri.dicom.root = 'flat';
        matlabbatch{10}.spm.tools.hmri.dicom.outdir = {fullfile(out_dir, 'B0', 'phase')};
        matlabbatch{10}.spm.tools.hmri.dicom.protfilter = '.*';
        matlabbatch{10}.spm.tools.hmri.dicom.convopts.format = 'nii';
        matlabbatch{10}.spm.tools.hmri.dicom.convopts.metaopts.mformat = 'sep';
        matlabbatch{10}.spm.tools.hmri.dicom.convopts.icedims = 0;

        save(batchfile, 'matlabbatch');

        % run directly
        %spm_jobman('run', matlabbatch);
        % submit as job
        system(sprintf('sbatch -D. -c 1 -J %s -o %s --wrap "%s"', ...
                       ['dcms2nifti-', legacy_subject], ...
                       batchlog, batchcmd));
    end
end
stop_log(log_file);
