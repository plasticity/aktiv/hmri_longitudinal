addpath('lib')
addpath(fullfile('lib', 'bids'))

setup_paths
warning('off', 'MATLAB:MKDIR:DirectoryExists');
log_file = start_log(fullfile(deriv_path, 'logs'), mfilename);
spm('defaults','FMRI');

% before further processing, carefully check segmentation results at this point and potentially rerun with uncleaned data to check for improvements
% there are 3 subjects for which segmentation does not work at all, so we remove their clean images from further processing
system(sprintf('rm %s', ...
    [deriv_path '/MPM_noreorient/sub-AKTIV1102/ses-B/qMRImaps/Results/*c?sub-AKTIV1102ses-B_MTsat_clean.nii']));
system(sprintf('rm %s', ...
    [deriv_path '/MPM_noreorient/sub-AKTIV3130/ses-C/qMRImaps/Results/*c?sub-AKTIV3130ses-C_MTsat_clean.nii']));
system(sprintf('rm %s', ...
    [deriv_path '/MPM_noreorient/sub-AKTIV3232/ses-B/qMRImaps/Results/*c?sub-AKTIV3232ses-B_MTsat_clean.nii']));

subjects = get_subjects(nifti_path, 'full');
for i=1:length(subjects)
    subject = cell2mat(subjects(i));
    sessions = get_sessions(nifti_path, subject);
    if isfile(spm_select('FPList', fullfile(deriv_path, 'MPM_noreorient', subject, sessions{1}, 'qMRImaps', 'Results'), '^Template_4\.nii$'))
        fprintf('Template already created, skipping %s\n', subject);
    else
        clear TP_rc1 TP_rc2 files
        for j=1:length(sessions)
            session = cell2mat(sessions(j));
            tmpid = get_tmpid_s();
            in_dir = fullfile(deriv_path, 'MPM_noreorient', subject, session);
            out_dir = fullfile(deriv_path, 'MPM_noreorient', subject);
            mkdir(fullfile(out_dir, 'batches'));
            mkdir(fullfile(out_dir, 'logs'));
            if isfile(spm_select('FPList', fullfile(in_dir, 'qMRImaps', 'Results'), '^rc1.*_MTsat_clean\.nii$'))
                TP_rc1(j,:)  = spm_select('FPList', fullfile(in_dir, 'qMRImaps', 'Results'), '^rc1.*_MTsat_clean\.nii$');
                TP_rc2(j,:)  = spm_select('FPList', fullfile(in_dir, 'qMRImaps', 'Results'), '^rc2.*_MTsat_clean\.nii$');
            else
                fprintf('Timepoint %s not available for subject %s\n', session, subject);
            end
        end
        if isempty(TP_rc1)
            fprintf('No timepoints available for subject %s\n', subject);
        else
            files = {cellstr(TP_rc1), cellstr(TP_rc2)};
            files=files(~cellfun('isempty',files));
            batchfile = fullfile(out_dir, 'batches', [subject, '_longreg-', tmpid, '.mat']);
            batchlog = fullfile(out_dir, 'logs', [subject, '_longreg-%j-', tmpid, '.log']);
            batchcmd = sprintf(['. /etc/profile; module unload matlab/R2017b; module load matlab/R2017b; matlab -r \\"' ...
                'addpath(''%s''); ' ...
                'addpath(genpath(''%s'')); ' ...
                'spm(''defaults'', ''fmri''); ' ...
                'spm_jobman(''initcfg''); ' ...
                'load(''%s''); ' ...
                'spm_jobman(''run'', matlabbatch); ' ...
                'quit;\\"'], ...
                spm_with_hmri, ...
                fullfile(spm_with_hmri, 'toolbox', 'hMRI'), ...
                batchfile);
            
            fprintf('Creating batch for  %s ...\n', subject);
            clear matlabbatch
            matlabbatch{1}.spm.tools.shoot.warp.images = files;
            
            save(batchfile, 'matlabbatch');
            
            fprintf('Submitting\n');
            system(sprintf('sbatch -D. -c 1 -J %s -o %s --wrap "%s"', ...
                ['shoot_', subject], batchlog, batchcmd));
        end
    end
end %subject

stop_log(log_file);

