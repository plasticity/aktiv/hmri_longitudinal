% user specific
home          = getenv('HOME');
base_path     = fullfile(home, 'users', 'wenger', 'hmri', 'output');
deriv_path    = fullfile(base_path, 'derivatives');
spm_with_hmri = fullfile(home, 'matlab', 'tools', 'spm12_hmri');
compiled_hmri = fullfile(home, 'matlab', 'tools', 'spm12_hmri_standalone', 'run_spm12.sh');
%nifti_path    = fullfile(base_path, 'rawdata');  % your own nifti copy if you intend to re-import

% system specific
mcr_path      = '/opt/matlab/R2017b'; % needs to match compiled_hmri
input_path    = '/home/beegfs/BIDS/LIP/Plasticity/Aktiv/';
dicom_path    = fullfile(input_path, 'sourcedata');
nifti_path    = fullfile(input_path, 'rawdata'); % cluster-copy of the niftis

% load path
addpath(spm_with_hmri);
addpath(genpath(fullfile(spm_with_hmri, 'toolbox', 'hMRI')));
