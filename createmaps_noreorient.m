addpath('lib')
addpath(fullfile('lib', 'bids'))

setup_paths
warning('off', 'MATLAB:MKDIR:DirectoryExists');
log_file = start_log(fullfile(deriv_path, 'logs'), mfilename);
spm('defaults','FMRI');

subjects = get_subjects(nifti_path, 'full');
for i=1:length(subjects)
    subject = cell2mat(subjects(i));
    sessions = get_sessions(nifti_path, subject);
    for j=1:length(sessions)
        session = cell2mat(sessions(j));
        tmpid = get_tmpid_s();
        in_dir = fullfile(nifti_path, subject, session);
        out_dir = fullfile(deriv_path, 'MPM_noreorient', subject);
        mkdir (fullfile(out_dir));
        mkdir (fullfile(out_dir, 'batches'));
        mkdir(fullfile(out_dir, 'logs'));
        batchfile = fullfile(out_dir, 'batches', [subject, '_', session, '_createmaps-', tmpid, '.mat']);
        batchlog = fullfile(out_dir, 'logs', [subject, '_', session, '_createmaps-%j-', tmpid, '.log']);
%        batchcmd = sprintf('%s %s batch %s', compiled_hmri, mcr_path, batchfile);
        batchcmd = sprintf(['. /etc/profile; module unload matlab/R2017b; module load matlab/R2017b; matlab -r \\"' ...
            'addpath(''%s''); ' ...
            'addpath(genpath(''%s'')); ' ...
            'spm(''defaults'', ''fmri''); ' ...
            'spm_jobman(''initcfg''); ' ...
            'load(''%s''); ' ...
            'spm_jobman(''run'', matlabbatch); ' ...
            'quit;\\"'], ...
            spm_with_hmri, ...
            fullfile(spm_with_hmri, 'toolbox', 'hMRI'), ...
            batchfile);
        files=cell(1,5);
        
        if exist(fullfile(out_dir, session, 'qMRImaps', 'Results', '_finished_'))
            fprintf('qMRImaps already exist, skipping %s/%s\n', subject, session);
        else
            
            files{1} = spm_select('FPList', fullfile(in_dir, 'B1mapping'), '^.*\.nii$');
            
            b0m = spm_select('FPList', fullfile(in_dir, 'B0', 'magnitude'), '^.*\.nii$');
            b0p = spm_select('FPList', fullfile(in_dir, 'B0', 'phase'), '^.*\.nii$');
            files{2} = vertcat(cellstr(b0m), cellstr(b0p));
            
            files{3} = spm_select('FPList', fullfile(in_dir, 'MT', 'magnitude'), '^.*\.nii$');
            files{4} = spm_select('FPList', fullfile(in_dir, 'PD', 'magnitude'), '^.*\.nii$');
            files{5} = spm_select('FPList', fullfile(in_dir, 'T1', 'magnitude'), '^.*\.nii$');

            
            
            fprintf('Creating batch for %s/%s ...\n', subject, session);
            clear matlabbatch
            
            matlabbatch{1}.spm.tools.hmri.create_mpm.subj.output.outdir = {fullfile(out_dir, session, 'qMRImaps')};
            matlabbatch{1}.spm.tools.hmri.create_mpm.subj.sensitivity.RF_us = '-';
            matlabbatch{1}.spm.tools.hmri.create_mpm.subj.b1_type.i3D_EPI.b1input = cellstr(files{1});
            matlabbatch{1}.spm.tools.hmri.create_mpm.subj.b1_type.i3D_EPI.b0input = files{2};
            matlabbatch{1}.spm.tools.hmri.create_mpm.subj.b1_type.i3D_EPI.b1parameters.b1metadata = 'yes';
            matlabbatch{1}.spm.tools.hmri.create_mpm.subj.raw_mpm.MT = cellstr(files{3});
            matlabbatch{1}.spm.tools.hmri.create_mpm.subj.raw_mpm.PD = cellstr(files{4});
            matlabbatch{1}.spm.tools.hmri.create_mpm.subj.raw_mpm.T1 = cellstr(files{5});
            matlabbatch{1}.spm.tools.hmri.create_mpm.subj.popup = false;
            
            save(batchfile, 'matlabbatch');
            
            fprintf('Submitting\n');
            %system(sprintf('module unload matlab/R2017b'));
            system(sprintf('sbatch -D. -c 1 -J %s -o %s --wrap "%s"', ...
                ['createmaps_', subject, '_', session], ...
                batchlog, batchcmd));
        end
    end % session
end % subject
stop_log(log_file);
