addpath('lib')
addpath(fullfile('lib', 'bids'))

setup_paths
warning('off', 'MATLAB:MKDIR:DirectoryExists');
log_file = start_log(fullfile(deriv_path, 'logs'), mfilename);
spm('defaults','FMRI');

subjects = get_subjects(nifti_path, 'full');
for i=1:length(subjects)
    subject = cell2mat(subjects(i));
    sessions = get_sessions(nifti_path, subject);
    for j=1:length(sessions)
        session = cell2mat(sessions(j));
        if isfile(spm_select('FPList', fullfile(deriv_path, 'MPM_noreorient', subject, session, 'qMRImaps', 'Results'), '^reg2Temp.*\MTsat\.nii$'))
            fprintf('Already transformed into average space, skipping %s %s\n', subject, session);
        elseif exist(fullfile(deriv_path, 'MPM_noreorient', subject, session, 'qMRImaps', 'Results', 'B1mapCalc'), 'dir')
            fprintf('Map creation unsuccessful, skipping %s %s\n', subject, session);
        elseif ~isfile(spm_select('FPList', fullfile(deriv_path, 'MPM_noreorient', subject, session, 'qMRImaps', 'Results'), '^y_rc1.*\MTsat_clean_Template\.nii$'))
            fprintf('No transformation to subject template available, skipping %s %s\n', subject, session);
        else
            tmpid = get_tmpid_s();
            in_dir = fullfile(deriv_path, 'MPM_noreorient', subject, session);
            out_dir = fullfile(deriv_path, 'MPM_noreorient', subject);
            mkdir(fullfile(out_dir, 'batches'));
            mkdir(fullfile(out_dir, 'logs'));
            batchfile = fullfile(out_dir, 'batches', [subject, '_', session, '_apply_def-', tmpid, '.mat']);
            batchlog = fullfile(out_dir, 'logs', [subject, '_', session, '_apply_def-%j-', tmpid, '.log']);
            batchcmd = sprintf('%s %s batch %s', compiled_hmri, mcr_path, batchfile);
            
            def = spm_select('FPList', fullfile(in_dir, 'qMRImaps', 'Results'), '^y_rc1.*\MTsat_clean_Template\.nii$');
            def = cellstr(def);
            
            map1 = spm_select('FPList', fullfile(in_dir, 'qMRImaps', 'Results'), '^s.*\MTsat\.nii$');
            map2 = spm_select('FPList', fullfile(in_dir, 'qMRImaps', 'Results'), '^s.*\PD\.nii$');
            MT = { map1};
            PD = { map2};
            
            fprintf('Creating batch for %s/%s ...\n', subject, session);
            clear matlabbatch
            matlabbatch{1}.spm.util.defs.comp{1}.def = def;
            matlabbatch{1}.spm.util.defs.out{1}.pull.fnames = MT;
            matlabbatch{1}.spm.util.defs.out{1}.pull.savedir.savesrc = 1;
            matlabbatch{1}.spm.util.defs.out{1}.pull.interp = 4;
            matlabbatch{1}.spm.util.defs.out{1}.pull.mask = 1;
            matlabbatch{1}.spm.util.defs.out{1}.pull.fwhm = [0 0 0];
            matlabbatch{1}.spm.util.defs.out{1}.pull.prefix = 'reg2Temp';

            matlabbatch{2}.spm.util.defs.comp{1}.def = def;
            matlabbatch{2}.spm.util.defs.out{1}.pull.fnames = PD;
            matlabbatch{2}.spm.util.defs.out{1}.pull.savedir.savesrc = 1;
            matlabbatch{2}.spm.util.defs.out{1}.pull.interp = 4;
            matlabbatch{2}.spm.util.defs.out{1}.pull.mask = 1;
            matlabbatch{2}.spm.util.defs.out{1}.pull.fwhm = [0 0 0];
            matlabbatch{2}.spm.util.defs.out{1}.pull.prefix = 'reg2Temp';

            save(batchfile, 'matlabbatch');
            
            fprintf('Submitting\n');
            system(sprintf('sbatch -D. -c 1 -J %s -o %s --wrap "%s"', ...
                ['apply_def_', subject, '_', session], ...
                batchlog, batchcmd));
        end
    end % session
end % subject
stop_log(log_file);
