addpath('lib')
addpath(fullfile('lib', 'bids'))
setup_paths
log_file = start_log(fullfile(deriv_path, 'logs'), mfilename);

dups = dir(fullfile(nifti_path, 'sub-AKTIV*_1'));
dups = {dups.name};
for i=1:length(dups)
    % duplicates
    subject_dup = dups{i};
    % corresponding original
    subject = replace(subject_dup, '_1', '');
    % the session we need to merge somehow
    sessions = get_sessions(nifti_path, subject_dup);
    session = sessions{1}; % there is only one session for each duplicate, I checked
    % we create a full, updated copy of a session under <subject>_1/ and then
    % copy back the full session to <subject>/, so make sure this lists
    % _all_ possible sequences here
    sequences = {
        fullfile('MPrage_3Ddist'),
        fullfile('B0', 'magnitude'),
        fullfile('B0', 'phase'),
        fullfile('B1mapping'),
        fullfile('MT', 'magnitude')
        fullfile('MT', 'phase')
        fullfile('PD', 'magnitude')
        fullfile('PD', 'phase')
        fullfile('T1', 'magnitude')
        fullfile('T1', 'phase')
    };

    fprintf('\n%-20s : %s vs %s\n', 'subject', subject, subject_dup)

    % PRE CHECKS
    for j=1:length(sequences)
        sequence = cell2mat(sequences(j));
        subject_ln = get_ln(nifti_path, subject, session, sequence);
        subject_dup_ln = get_ln(nifti_path, subject_dup, session, sequence);
        fprintf('FROM %-15s : %s vs %s\n', sequence, subject_ln, subject_dup_ln);
    end % sequences

    % MERGE
    disp('Merging data');
    for j=1:length(sequences)
        sequence = cell2mat(sequences(j));
        subject_ln = get_ln(nifti_path, subject, session, sequence);
        subject_dup_ln = get_ln(nifti_path, subject_dup, session, sequence);
        % replace(delete and copy) sequence from duplicate back to original if duplicate has data
        if subject_dup_ln ~= '____'
            source = fullfile(nifti_path, subject_dup, session, sequence);
            dest = fullfile(nifti_path, subject, session, sequence);
            rmdir(dest, 's');
            copyfile(source, dest);
        end
    end

    % POST CHECKS
    for j=1:length(sequences)
        sequence = cell2mat(sequences(j));
        % the original folder should now be "complete" with contiguous LN's
        subject_ln = get_ln(nifti_path, subject, session, sequence);
        subject_dup_ln = get_ln(nifti_path, subject_dup, session, sequence);
        fprintf('TO   %-15s : %s vs %s\n', sequence, subject_ln, subject_dup_ln);
    end

    % CLEANUP for whole subjects
    disp('Rescuing logs and batches from duplicate');
    copyfile(fullfile(nifti_path, subject_dup, session, 'logs'), ...
             fullfile(nifti_path, subject, session, 'logs'));
    copyfile(fullfile(nifti_path, subject_dup, session, 'batches'), ...
             fullfile(nifti_path, subject, session, 'batches'));
    rmdir(fullfile(nifti_path, subject_dup), 's');
end % subject
stop_log(log_file);


function ln = get_ln(nifti_path, subject, session, sequence)
    files = dir(fullfile(nifti_path, subject, session, sequence, '*.nii'));
    if length(files) > 0
        first_ni = files(1).name;
        splits = strsplit(first_ni, '-');
        ln = cell2mat(splits(2));
    else
        ln = '____';
    end
end
