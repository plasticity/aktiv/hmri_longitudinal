addpath('lib')
addpath(fullfile('lib', 'bids'))

setup_paths
warning('off', 'MATLAB:MKDIR:DirectoryExists');
log_file = start_log(fullfile(deriv_path, 'logs'), mfilename);
spm('defaults','FMRI');

subjects = get_subjects(nifti_path, 'full');

for i=1:length(subjects)
    subject = cell2mat(subjects(i));
    sessions = get_sessions(nifti_path, subject);
    for j=1:length(sessions)
        session = cell2mat(sessions(j));
        if isfile(spm_select('FPList', fullfile(deriv_path, 'MPM_noreorient', subject, session, 'qMRImaps', 'Results'), '^wap1.*\MTsat\.nii$'))
            fprintf('Smoothing has already run, skipping %s %s\n', subject, session);
        elseif exist(fullfile(deriv_path, 'MPM_noreorient', subject, session, 'qMRImaps', 'Results', 'B1mapCalc'), 'dir')
            fprintf('Map creation unsuccessful, skipping %s %s\n', subject, session);
        elseif ~isfile(spm_select('FPList', fullfile(deriv_path, 'MPM_noreorient', subject, session, 'qMRImaps', 'Results'), '^ws.*\MTsat\.nii$'))
            fprintf('Normalized date not available for subject %s %s\n', session, subject);
        else
            % copy the subject template into each session folder so there is no
            % conflict when being processed by multiple jobs
            system(sprintf('cp %s %s', ...
                [deriv_path '/MPM_noreorient/' subject '/mwc?median_' subject '_MTsat.nii'], ...
                [deriv_path '/MPM_noreorient/' subject '/' session]));
            
            tmpid = get_tmpid_s();
            in_dir = fullfile(deriv_path, 'MPM_noreorient', subject, session);
            out_dir = fullfile(deriv_path, 'MPM_noreorient', subject);
            mkdir(fullfile(out_dir, 'batches'));
            mkdir(fullfile(out_dir, 'logs'));
            batchfile = fullfile(out_dir, 'batches', [subject, '_', session, '_smooth-%j-', tmpid, '.mat']);
            batchlog = fullfile(out_dir, 'logs', [subject, '_', session, '_smooth-%j-', tmpid, '.log']);
            batchcmd = sprintf('%s %s batch %s', compiled_hmri, mcr_path, batchfile);
            
            clear map1 map2 map3 map4 mwc1 mwc2 files_map files_mwc
            map1 = spm_select('FPList', fullfile(in_dir, 'qMRImaps', 'Results'), '^ws.*\MTsat\.nii$');
            map2 = spm_select('FPList', fullfile(in_dir, 'qMRImaps', 'Results'), '^ws.*\PD\.nii$');
            map3 = spm_select('FPList', fullfile(in_dir, 'qMRImaps', 'Results'), '^ws.*\R1\.nii$');
            map4 = spm_select('FPList', fullfile(in_dir, 'qMRImaps', 'Results'), '^ws.*\R2s_OLS\.nii$');
            
            mwc1 = spm_select('FPList', in_dir, '^mwc1median.*\MTsat\.nii$');
            mwc2 = spm_select('FPList', in_dir, '^mwc2median.*\MTsat\.nii$');
            
            files_map = { cellstr(map1), cellstr(map2), cellstr(map3), cellstr(map4) };
            files_mwc = { cellstr(mwc1), cellstr(mwc2) };
            
            fprintf('Creating smoothing batch for %s %s ...\n', subject, session);
            clear matlabbatch
            matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_smooth.vols_pm = files_map;
            matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_smooth.vols_mwc = files_mwc;
            matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_smooth.tpm = {fullfile(spm_with_hmri, 'toolbox', 'hMRI' , 'etpm', 'eTPM.nii')};
            matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_smooth.fwhm = [6 6 6];
            matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_smooth.output.indir = 1;
            
            save(batchfile, 'matlabbatch');
            
            fprintf('Submitting\n');
            system(sprintf('sbatch -D. -c 1 -J %s -o %s --wrap "%s"', ...
                'smooth', batchlog, batchcmd));
            
        end %if
        
    end %session
    
end %subject

stop_log(log_file);
