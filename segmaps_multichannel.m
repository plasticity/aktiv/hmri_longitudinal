addpath('lib')
addpath(fullfile('lib', 'bids'))

setup_paths
warning('off', 'MATLAB:MKDIR:DirectoryExists');
log_file = start_log(fullfile(deriv_path, 'logs'), mfilename);
spm('defaults','FMRI');

% there are 2 subjects for which segmentation only works on the raw images, so we rename the raw images to clean so they run with the same script
system(sprintf('cp %s %s', ...
    [deriv_path '/MPM_noreorient/sub-AKTIV2239/ses-B/qMRImaps/Results/sA*MTsat.nii'], ...
    [deriv_path '/MPM_noreorient/sub-AKTIV2239/ses-B/qMRImaps/Results/sub-AKTIV2239ses-B_MTsat_clean.nii']));
system(sprintf('cp %s %s', ...
    [deriv_path '/MPM_noreorient/sub-AKTIV2239/ses-B/qMRImaps/Results/sA*PD.nii'], ...
    [deriv_path '/MPM_noreorient/sub-AKTIV2239/ses-B/qMRImaps/Results/sub-AKTIV2239ses-B_PD_clean.nii']));
system(sprintf('cp %s %s', ...
    [deriv_path '/MPM_noreorient/sub-AKTIV4125/ses-B/qMRImaps/Results/sA*MTsat.nii'], ...
    [deriv_path '/MPM_noreorient/sub-AKTIV4125/ses-B/qMRImaps/Results/sub-AKTIV4125ses-B_MTsat_clean.nii']));
system(sprintf('cp %s %s', ...
    [deriv_path '/MPM_noreorient/sub-AKTIV4125/ses-B/qMRImaps/Results/sA*PD.nii'], ...
    [deriv_path '/MPM_noreorient/sub-AKTIV4125/ses-B/qMRImaps/Results/sub-AKTIV4125ses-B_PD_clean.nii']));

subjects = get_subjects(nifti_path, 'full');
for i=1:length(subjects)
    subject = cell2mat(subjects(i));
    sessions = get_sessions(nifti_path, subject);
    for j=1:length(sessions)
        session = cell2mat(sessions(j));
        if isfile(spm_select('FPList', fullfile(deriv_path, 'MPM_noreorient', subject, session, 'qMRImaps', 'Results'), '^c1s.*\MTsat_clean\.nii$'))
            fprintf('Already segmented MTmap, skipping %s %s\n', subject, session);
        elseif exist(fullfile(deriv_path, 'MPM_noreorient', subject, session, 'qMRImaps', 'Results', 'B1mapCalc'), 'dir')
            fprintf('Map creation unsuccessful, skipping %s %s\n', subject, session);
        elseif ~isfile(spm_select('FPList', fullfile(deriv_path, 'MPM_noreorient', subject, session, 'qMRImaps', 'Results'), '^s.*\MTsat_clean\.nii$'))
            fprintf('No cleaned data available, skipping %s %s\n', subject, session);
        else
            tmpid = get_tmpid_s();
            in_dir = fullfile(deriv_path, 'MPM_noreorient', subject, session, 'qMRImaps', 'Results');
            out_dir = fullfile(deriv_path, 'MPM_noreorient', subject);
            mkdir(fullfile(out_dir, 'batches'));
            mkdir(fullfile(out_dir, 'logs'));
            batchfile = fullfile(out_dir, 'batches', [subject, '_', session, '_segment-', tmpid, '.mat']);
            batchlog = fullfile(out_dir, 'logs', [subject, '_', session, '_segment-%j-', tmpid, '.log']);
            batchcmd = sprintf('%s %s batch %s', compiled_hmri, mcr_path, batchfile);
            
            clear file_MT file_PD
            file_MT = spm_select('FPList', in_dir, '^s.*MTsat_clean\.nii$');
            file_PD = spm_select('FPList', in_dir, '^s.*PD_clean\.nii$');
            
            fprintf('Creating batch for %s %s ...\n', subject, session);
            clear matlabbatch
            matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.many_sdatas.output.indir = 1;
            matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.many_sdatas.channel(1).vols = {file_MT};
            matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.many_sdatas.channel(1).biasreg = 1;
            matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.many_sdatas.channel(1).biasfwhm = Inf;
            matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.many_sdatas.channel(1).write = [0 0];
            matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.many_sdatas.channel(2).vols = {file_PD};
            matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.many_sdatas.channel(2).biasreg = 0.001;
            matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.many_sdatas.channel(2).biasfwhm = Inf;
            matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.many_sdatas.channel(2).write = [0 0];
            matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.many_sdatas.vols_pm = {};
            matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.many_sdatas.vox = [1 1 1];
            matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.many_sdatas.bb = [-78 -112 -70
                78 76 85];
            matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.tissue(1).tpm = {fullfile(spm_with_hmri, 'toolbox', 'hMRI' , 'etpm', 'eTPM.nii,1')};
            matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.tissue(1).ngaus = 2;
            matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.tissue(1).native = [1 1];
            matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.tissue(1).warped = [0 0];
            matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.tissue(2).tpm = {fullfile(spm_with_hmri, 'toolbox', 'hMRI' , 'etpm', 'eTPM.nii,2')};
            matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.tissue(2).ngaus = 2;
            matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.tissue(2).native = [1 1];
            matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.tissue(2).warped = [0 0];
            matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.tissue(3).tpm = {fullfile(spm_with_hmri, 'toolbox', 'hMRI' , 'etpm', 'eTPM.nii,3')};
            matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.tissue(3).ngaus = 2;
            matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.tissue(3).native = [1 0];
            matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.tissue(3).warped = [0 0];
            matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.tissue(4).tpm = {fullfile(spm_with_hmri, 'toolbox', 'hMRI' , 'etpm', 'eTPM.nii,4')};
            matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.tissue(4).ngaus = 3;
            matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.tissue(4).native = [0 0];
            matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.tissue(4).warped = [0 0];
            matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.tissue(5).tpm = {fullfile(spm_with_hmri, 'toolbox', 'hMRI' , 'etpm', 'eTPM.nii,5')};
            matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.tissue(5).ngaus = 4;
            matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.tissue(5).native = [0 0];
            matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.tissue(5).warped = [0 0];
            matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.tissue(6).tpm = {fullfile(spm_with_hmri, 'toolbox', 'hMRI' , 'etpm', 'eTPM.nii,6')};
            matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.tissue(6).ngaus = 2;
            matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.tissue(6).native = [0 0];
            matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.tissue(6).warped = [0 0];
            matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.warp.mrf = 1;
            matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.warp.cleanup = 1;
            matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.warp.reg = [0 0.001 0.5 0.05 0.2];
            matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.warp.affreg = 'mni';
            matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.warp.fwhm = 0;
            matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.warp.samp = 3;
            matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.warp.write = [0 1];
            matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.warp.vox = NaN;
            matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.warp.bb = [NaN NaN NaN
                NaN NaN NaN];
            
            save(batchfile, 'matlabbatch');
            
            fprintf('Submitting\n');
            system(sprintf('sbatch -D. -c 1 -J %s -o %s --wrap "%s"', ...
                ['segment_', subject, session], ...
                batchlog, batchcmd));
        end
    end %session
end % subject
stop_log(log_file);
