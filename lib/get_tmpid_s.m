function id = get_tmpid_s()
    %
    % There is java.util.UUID.randomUUID, but we don't always have java.
    % Also, this must not be cryptographically secure, so we just use a part of
    % tempname().
    %
    name = tempname();
    [~, name] = fileparts(tempname());
    parts = strsplit(name, '_');
    id = sprintf("%s%s", parts{1}, parts{2}); % 16**8 should be readable enough and "unique" enough here
    id = cell2mat(id);
end
