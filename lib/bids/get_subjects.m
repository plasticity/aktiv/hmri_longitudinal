function subjects = get_subjects(base_path, mode)
    %
    % SUBJECTS = get_subjects(BASE_PATH, MODE)
    %
    % Returns a list of SUBJECT identifiers found in a BASE_PATH of a bids dataset.
    %
    % If the MODE parameter is set to 'test', only return the first three. This is
    % helpful for debugging.

    subjects = dir(fullfile(base_path, 'sub-*'));
    subjects = {subjects.name};

    if strcmp(mode, 'test')
        subjects = subjects(1:3);
    end
end
