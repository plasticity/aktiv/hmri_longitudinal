function sessions = get_sessions(base_path, subject)
    %
    % SESSIONS = get_subjects(BASE_PATH, SUBJECT)
    %
    % Returns a list of SESSION identifiers for a given SUBJECT found in
    % a BASE_PATH of a bids dataset.
    %
    sessions = dir(fullfile(base_path, subject, 'ses-*'));
    sessions = {sessions.name};
end
