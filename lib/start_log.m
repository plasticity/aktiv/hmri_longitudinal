function log_file = start_log(log_dir, name)
    %
    % Start a log using diary() in a logfile with a unique, ascending ID
    % conforming to the pattern:
    %
    %   <name>_000X.log
    %
    % in some base directory log_dir.
    %
    if ~exist(log_dir)
        mkdir(log_dir);
    end
    existing_logs = dir(fullfile(log_dir, [name, '*.log']));
    if length(existing_logs)
        latest_log = existing_logs(length(existing_logs)).name;
        base = replace(latest_log, '.log', '');
        log_num = replace(base, [name, '_'], '');
        log_num = str2num(log_num) + 1;
    else
        log_num = 1;
    end
    log_file = sprintf('%s_%04d.log', name, log_num);
    log_file = fullfile(log_dir, log_file);
    diary(log_file);
    fprintf('Starting log at: %s\n', datestr(now));
end
