function stop_log(logfile)
    %
    % Stop the diary and implicitly close the logfile. Also, make it read-only.
    %
    diary off
    if ispc
        fileattrib(logfile, '-w');
    else
        fileattrib(logfile, '-w', 'a');
    end
end
