function subjects = get_subjects(dicom_path, mode)
    %
    % SUBJECTS = get_subjects(DICOM_PATH, MODE)
    %
    % Return the set union of all AKTIVXXXX IDs found in some hard coded DICOM_PATH
    % subdirectories.
    %
    % If the MODE parameter is set to 'test', only return the first three. This
    % helps with debugging.
    %
    subjects_b1 = dir(fullfile(dicom_path, 'MPM', 'B1mapping', 'AKTIV*'));
    subjects_mp = dir(fullfile(dicom_path, 'MPM', 'MPrage_3Ddist', 'AKTIV*'));
    subjects_mt = dir(fullfile(dicom_path, 'MPM', 'MT', 'AKTIV*'));
    subjects_pd = dir(fullfile(dicom_path, 'MPM', 'PD', 'AKTIV*'));
    subjects_t1 = dir(fullfile(dicom_path, 'MPM', 'T1', 'AKTIV*'));
    subjects_b0 = dir(fullfile(dicom_path, 'GRE_Field', 'B0', 'AKTIV*'));

    subjects = union({subjects_b1.name}, {subjects_mp.name});
    subjects = union(subjects, {subjects_mt.name});
    subjects = union(subjects, {subjects_pd.name});
    subjects = union(subjects, {subjects_t1.name});
    subjects = union(subjects, {subjects_b0.name});

    if strcmp(mode, 'test')
        subjects = subjects(1:13);
    end
end
