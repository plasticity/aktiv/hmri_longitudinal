function [subject, session]  = get_sessions(aktiv_subject_string)
    %
    % [SUBJECT, SESSION] = get_sessions(AKTIV_SUBJECT_STRING)
    %
    % Returns a SUBJECT identifier (bids) and a SESSION identifier given an
    % AKTIV_ID.
    %
    % Examples:
    %
    %  AKTIV1101A   -> [sub-AKTIV1101, ses-A]
    %  AKTIV1101B_1 -> [sub-AKTIV1101_1, ses-B]
    %
    % Originially, subject names include the timepoint (A,B,C) and a suffix
    % `_1`, if there was a second acquisition ("run") We decided not to include
    % the `run_N` label in the BIDS-like import and just use the run that has
    % a complete set of modalities by merging subjects X and X_1 in a later
    % step.
    %
    %
    tokens = regexp(aktiv_subject_string, '.*(AKTIV[0-9]+)([A-C]{1})(_1)?', 'tokens');
    if length(tokens{1}) ~= 3
        error(sprintf('found unexpected subject name: %s',aktiv_subject_string));
    end
    subject = ['sub-', cell2mat(tokens{1}(1))];
    session = ['ses-', cell2mat(tokens{1}(2))];
    dupl = cell2mat(tokens{1}(3));
    if length(dupl) > 0
        subject = [subject, dupl];
    end
end
