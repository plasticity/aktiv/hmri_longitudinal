addpath('lib')
addpath(fullfile('lib', 'bids'))

setup_paths
warning('off', 'MATLAB:MKDIR:DirectoryExists');
log_file = start_log(fullfile(deriv_path, 'logs'), mfilename);
spm('defaults','FMRI');

subjects =  get_subjects(nifti_path, 'full');
for i=1:length(subjects)
    subject = cell2mat(subjects(i));
    sessions = get_sessions(nifti_path, subject);
    if isfile(spm_select('FPList', fullfile(deriv_path, 'MPM_noreorient', subject), '^median.*\MTsat\.nii$'))
        fprintf('Already created median, skipping %s\n', subject);
    else
        clear TP_MT TP_PD files_MT files_PD
        for j=1:length(sessions)
            session = cell2mat(sessions(j));
            tmpid = get_tmpid_s();
            in_dir = fullfile(deriv_path, 'MPM_noreorient', subject, session);
            out_dir = fullfile(deriv_path, 'MPM_noreorient', subject);
            mkdir(fullfile(out_dir, 'batches'));
            mkdir(fullfile(out_dir, 'logs'));
            if isfile(spm_select('FPList', fullfile(in_dir, 'qMRImaps', 'Results'), '^reg2Temp.*\MTsat\.nii$'))
                TP_MT(j,:)  = spm_select('FPList', fullfile(in_dir, 'qMRImaps', 'Results'), '^reg2Temp.*\MTsat\.nii$');
                TP_PD(j,:)  = spm_select('FPList', fullfile(in_dir, 'qMRImaps', 'Results'), '^reg2Temp.*\PD\.nii$');
            else
                fprintf('Timepoint %s not available for subject %s\n', session, subject);
            end
        end
        if isempty(TP_MT)
            fprintf('No timepoints available for subject %s\n', subject);
        else
            
            files_MT{1} = cellstr(TP_MT);
            files_MT{1}=files_MT{1}(~cellfun('isempty',files_MT{1}));
            files_PD{1} = cellstr(TP_PD);
            files_PD{1}=files_PD{1}(~cellfun('isempty',files_PD{1}));
            batchfile = fullfile(out_dir, 'batches', [subject, '_comp_median-', tmpid, '.mat']);
            batchlog = fullfile(out_dir, 'logs', [subject, '_comp_median-%j-', tmpid, '.log']);
            batchcmd = sprintf('%s %s batch %s', compiled_hmri, mcr_path, batchfile);
            
            fprintf('Creating batch for %s ...\n', subject);
            clear matlabbatch
            
            % if participants have only one timepoint, we cannot compute a median, so we just reslice that image to have eual manipulations
            if size(files_MT{1},1) == 1
                matlabbatch{1}.spm.util.imcalc.input = files_MT{1};
                matlabbatch{1}.spm.util.imcalc.output = ['median_' subject '_MTsat'];
                matlabbatch{1}.spm.util.imcalc.outdir = {out_dir};
                matlabbatch{1}.spm.util.imcalc.expression = 'i1';
                matlabbatch{1}.spm.util.imcalc.var = struct('name', {}, 'value', {});
                matlabbatch{1}.spm.util.imcalc.options.dmtx = 0;
                matlabbatch{1}.spm.util.imcalc.options.mask = 0;
                matlabbatch{1}.spm.util.imcalc.options.interp = 1;
                matlabbatch{1}.spm.util.imcalc.options.dtype = 4;
                
                matlabbatch{2}.spm.util.imcalc.input = files_PD{1};
                matlabbatch{2}.spm.util.imcalc.output = ['median_' subject '_PD'];
                matlabbatch{2}.spm.util.imcalc.outdir = {out_dir};
                matlabbatch{2}.spm.util.imcalc.expression = 'i1';
                matlabbatch{2}.spm.util.imcalc.var = struct('name', {}, 'value', {});
                matlabbatch{2}.spm.util.imcalc.options.dmtx = 0;
                matlabbatch{2}.spm.util.imcalc.options.mask = 0;
                matlabbatch{2}.spm.util.imcalc.options.interp = 1;
                matlabbatch{2}.spm.util.imcalc.options.dtype = 4;
                
                % if they have multiple timepoints we compute the median
            else
                
                matlabbatch{1}.spm.util.imcalc.input = files_MT{1};
                matlabbatch{1}.spm.util.imcalc.output = ['median_' subject '_MTsat'];
                matlabbatch{1}.spm.util.imcalc.outdir = {out_dir};
                matlabbatch{1}.spm.util.imcalc.expression = 'median(X)';
                matlabbatch{1}.spm.util.imcalc.var = struct('name', {}, 'value', {});
                matlabbatch{1}.spm.util.imcalc.options.dmtx = 1;
                matlabbatch{1}.spm.util.imcalc.options.mask = 0;
                matlabbatch{1}.spm.util.imcalc.options.interp = 1;
                matlabbatch{1}.spm.util.imcalc.options.dtype = 4;
                
                matlabbatch{2}.spm.util.imcalc.input = files_PD{1};
                matlabbatch{2}.spm.util.imcalc.output = ['median_' subject '_PD'];
                matlabbatch{2}.spm.util.imcalc.outdir = {out_dir};
                matlabbatch{2}.spm.util.imcalc.expression = 'median(X)';
                matlabbatch{2}.spm.util.imcalc.var = struct('name', {}, 'value', {});
                matlabbatch{2}.spm.util.imcalc.options.dmtx = 1;
                matlabbatch{2}.spm.util.imcalc.options.mask = 0;
                matlabbatch{2}.spm.util.imcalc.options.interp = 1;
                matlabbatch{2}.spm.util.imcalc.options.dtype = 4;
                
            end
            
            save(batchfile, 'matlabbatch');
            
            fprintf('Submitting\n');
            system(sprintf('sbatch -D. -c 1 -J %s -o %s --wrap "%s"', ...
                ['median_', subject], ...
                batchlog, batchcmd));
        end
    end
end % subject
stop_log(log_file);

