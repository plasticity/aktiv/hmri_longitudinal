addpath('lib')
addpath(fullfile('lib', 'bids'))

setup_paths
warning('off', 'MATLAB:MKDIR:DirectoryExists');
log_file = start_log(fullfile(deriv_path, 'logs'), mfilename);
spm('defaults','FMRI');

subjects = get_subjects(nifti_path, 'full');
for i=1:length(subjects)
    subject = cell2mat(subjects(i));
    sessions = get_sessions(nifti_path, subject);
    for j=1:length(sessions)
        session = cell2mat(sessions(j));
        if isfile(spm_select('FPList', fullfile(deriv_path, 'MPM_noreorient', subject, session, 'qMRImaps', 'Results'), '^s.*\MTsat_clean\.nii$'))
            fprintf('Already thresholded maps, skipping %s %s\n', subject, session);
        elseif exist(fullfile(deriv_path, 'MPM_noreorient', subject, session, 'qMRImaps', 'Results', 'B1mapCalc'), 'dir')
            fprintf('Map creation unsuccessful, skipping %s %s\n', subject, session);
        elseif ~isfile(spm_select('FPList', fullfile(deriv_path, 'MPM_noreorient', subject, session, 'qMRImaps', 'Results'), '^s.*\MTsat\.nii$'))
            fprintf('Timepoint not available, skipping %s %s\n', subject, session);
        else
            tmpid = get_tmpid_s();
            in_dir = fullfile(deriv_path, 'MPM_noreorient', subject, session, 'qMRImaps', 'Results');
            out_dir = fullfile(deriv_path, 'MPM_noreorient', subject);
            mkdir(fullfile(out_dir, 'batches'));
            mkdir(fullfile(out_dir, 'logs'));
            batchfile = fullfile(out_dir, 'batches', [subject, '_', session, '_thresholdmaps-', tmpid, '.mat']);
            batchlog = fullfile(out_dir, 'logs', [subject, '_', session, '_thresholdmaps-%j-', tmpid, '.log']);
            batchcmd = sprintf('%s %s batch %s', compiled_hmri, mcr_path, batchfile);
            
            clear MT MTmap PD PDmap
            MT = spm_select('FPList', in_dir, '^s.*\MTsat\.nii$');
            MTmap = cellstr(MT);
            PD = spm_select('FPList', in_dir, '^s.*\PD\.nii$');
            PDmap = cellstr(PD);

            
            fprintf('Creating batch for %s/%s ...\n', subject, session);
            clear matlabbatch
            matlabbatch{1}.spm.util.imcalc.input = MTmap;
            matlabbatch{1}.spm.util.imcalc.output = [subject session '_MTsat_clean'];
            matlabbatch{1}.spm.util.imcalc.outdir = {in_dir};
            matlabbatch{1}.spm.util.imcalc.expression = 'i1.*(i1>0 & i1<5)';
            matlabbatch{1}.spm.util.imcalc.var = struct('name', {}, 'value', {});
            matlabbatch{1}.spm.util.imcalc.options.dmtx = 0;
            matlabbatch{1}.spm.util.imcalc.options.mask = 0;
            matlabbatch{1}.spm.util.imcalc.options.interp = 1;
            matlabbatch{1}.spm.util.imcalc.options.dtype = 16;
            
            matlabbatch{2}.spm.util.imcalc.input = PDmap;
            matlabbatch{2}.spm.util.imcalc.output = [subject session '_PD_clean'];
            matlabbatch{2}.spm.util.imcalc.outdir = {in_dir};
            matlabbatch{2}.spm.util.imcalc.expression = '(i1>0 & i1<2e2).*i1';
            matlabbatch{2}.spm.util.imcalc.var = struct('name', {}, 'value', {});
            matlabbatch{2}.spm.util.imcalc.options.dmtx = 0;
            matlabbatch{2}.spm.util.imcalc.options.mask = 0;
            matlabbatch{2}.spm.util.imcalc.options.interp = 1;
            matlabbatch{2}.spm.util.imcalc.options.dtype = 16;

            
            save(batchfile, 'matlabbatch');
            
            fprintf('Submitting\n');
            system(sprintf('sbatch -D. -c 1 -J %s -o %s --mem-per-cpu=10000 --wrap "%s"', ...
                ['thresholdmaps_', subject, '_', session], ...
                batchlog, batchcmd));
        end
    end % session
end % subject
stop_log(log_file);
