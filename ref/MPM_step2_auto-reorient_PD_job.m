%-----------------------------------------------------------------------
% Job saved on 31-Jan-2020 09:33:05 by cfg_util (rev $Rev: 7345 $)
% spm SPM - SPM12 (7487)
% cfg_basicio BasicIO - Unknown
%-----------------------------------------------------------------------
matlabbatch{1}.spm.tools.hmri.autoreor.reference = {'/Users/wenger/Desktop/MPM_reorient_tryout/AKTIV1209A/PD/sAKTIV1209A_6928-0073-00001-000176-01.nii,1'};
matlabbatch{1}.spm.tools.hmri.autoreor.template = {'/Users/wenger/Documents/spm12/canonical/avg152PD.nii,1'};
matlabbatch{1}.spm.tools.hmri.autoreor.other = {
                                                '/Users/wenger/Desktop/MPM_reorient_tryout/AKTIV1209A/PD/sAKTIV1209A_6928-0073-00001-000176-01.nii,1'
                                                '/Users/wenger/Desktop/MPM_reorient_tryout/AKTIV1209A/PD/sAKTIV1209A_6928-0073-00001-000352-02.nii,1'
                                                '/Users/wenger/Desktop/MPM_reorient_tryout/AKTIV1209A/PD/sAKTIV1209A_6928-0073-00001-000528-03.nii,1'
                                                '/Users/wenger/Desktop/MPM_reorient_tryout/AKTIV1209A/PD/sAKTIV1209A_6928-0073-00001-000704-04.nii,1'
                                                '/Users/wenger/Desktop/MPM_reorient_tryout/AKTIV1209A/PD/sAKTIV1209A_6928-0073-00001-000880-05.nii,1'
                                                '/Users/wenger/Desktop/MPM_reorient_tryout/AKTIV1209A/PD/sAKTIV1209A_6928-0073-00001-001056-06.nii,1'
                                                '/Users/wenger/Desktop/MPM_reorient_tryout/AKTIV1209A/PD/sAKTIV1209A_6928-0073-00001-001232-07.nii,1'
                                                '/Users/wenger/Desktop/MPM_reorient_tryout/AKTIV1209A/PD/sAKTIV1209A_6928-0073-00001-001408-08.nii,1'
                                                };
matlabbatch{1}.spm.tools.hmri.autoreor.output.outdir = {'/Users/wenger/Desktop/MPM_reorient_tryout/AKTIV1209A/PD'};
matlabbatch{1}.spm.tools.hmri.autoreor.dep = 'individual';
