%-----------------------------------------------------------------------
% Job saved on 02-Dec-2019 20:49:05 by cfg_util (rev $Rev: 7345 $)
% spm SPM - SPM12 (7487)
% cfg_basicio BasicIO - Unknown
%-----------------------------------------------------------------------
matlabbatch{1}.spm.tools.hmri.proc.proc_pipel.output.outdir = {'/Volumes/Plasticity/EnergI/Preprocessing/MPM/Processed/processed'};
matlabbatch{1}.spm.tools.hmri.proc.proc_pipel.s_vols = {
                                                        '/Volumes/Plasticity/EnergI/Preprocessing/Niftis/MPrage_3Ddist/AKTIV1209A/sAKTIV1209A_6928-0004-00001-000192-01.nii,1'
                                                        '/Volumes/Plasticity/EnergI/Preprocessing/Niftis/MPrage_3Ddist/AKTIV2121A/sAKTIV2121A_6987-0006-00001-000192-01.nii,1'
                                                        '/Volumes/Plasticity/EnergI/Preprocessing/Niftis/MPrage_3Ddist/AKTIV3107A/sAKTIV3107A_7096-0006-00001-000192-01.nii,1'
                                                        };
%%
matlabbatch{1}.spm.tools.hmri.proc.proc_pipel.vols_pm = {
                                                         {
                                                         '/Volumes/Plasticity/EnergI/Preprocessing/MPM/Processed/AKTIV1209A/Results/sAKTIV1209A_6928-0073-00001-000176-01_MTsat.nii,1'
                                                         '/Volumes/Plasticity/EnergI/Preprocessing/MPM/Processed/AKTIV2121A/Results/sAKTIV2121A_6987-0073-00001-000176-01_MTsat.nii,1'
                                                         '/Volumes/Plasticity/EnergI/Preprocessing/MPM/Processed/AKTIV3107A/Results/sAKTIV3107A_7096-0075-00001-000176-01_MTsat.nii,1'
                                                         }
                                                         {
                                                         '/Volumes/Plasticity/EnergI/Preprocessing/MPM/Processed/AKTIV1209A/Results/sAKTIV1209A_6928-0073-00001-000176-01_PD.nii,1'
                                                         '/Volumes/Plasticity/EnergI/Preprocessing/MPM/Processed/AKTIV2121A/Results/sAKTIV2121A_6987-0073-00001-000176-01_PD.nii,1'
                                                         '/Volumes/Plasticity/EnergI/Preprocessing/MPM/Processed/AKTIV3107A/Results/sAKTIV3107A_7096-0075-00001-000176-01_PD.nii,1'
                                                         }
                                                         {
                                                         '/Volumes/Plasticity/EnergI/Preprocessing/MPM/Processed/AKTIV1209A/Results/sAKTIV1209A_6928-0073-00001-000176-01_R1.nii,1'
                                                         '/Volumes/Plasticity/EnergI/Preprocessing/MPM/Processed/AKTIV2121A/Results/sAKTIV2121A_6987-0073-00001-000176-01_R1.nii,1'
                                                         '/Volumes/Plasticity/EnergI/Preprocessing/MPM/Processed/AKTIV3107A/Results/sAKTIV3107A_7096-0075-00001-000176-01_R1.nii,1'
                                                         }
                                                         {
                                                         '/Volumes/Plasticity/EnergI/Preprocessing/MPM/Processed/AKTIV1209A/Results/sAKTIV1209A_6928-0073-00001-000176-01_R2s_OLS.nii,1'
                                                         '/Volumes/Plasticity/EnergI/Preprocessing/MPM/Processed/AKTIV2121A/Results/sAKTIV2121A_6987-0073-00001-000176-01_R2s_OLS.nii,1'
                                                         '/Volumes/Plasticity/EnergI/Preprocessing/MPM/Processed/AKTIV3107A/Results/sAKTIV3107A_7096-0075-00001-000176-01_R2s_OLS.nii,1'
                                                         }
                                                         }';
%%
matlabbatch{1}.spm.tools.hmri.proc.proc_pipel.vox = [1 1 1];
matlabbatch{1}.spm.tools.hmri.proc.proc_pipel.bb = [-78 -112 -70
                                                    78 76 85];
matlabbatch{1}.spm.tools.hmri.proc.proc_pipel.fwhm = [6 6 6];
matlabbatch{1}.spm.tools.hmri.proc.proc_pipel.pipe_c = 2;
