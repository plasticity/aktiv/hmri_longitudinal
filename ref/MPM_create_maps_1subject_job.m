%-----------------------------------------------------------------------
% Job saved on 02-Dec-2019 14:21:53 by cfg_util (rev $Rev: 7345 $)
% spm SPM - SPM12 (7487)
% cfg_basicio BasicIO - Unknown
%-----------------------------------------------------------------------
matlabbatch{1}.spm.tools.hmri.create_mpm.subj.output.outdir = {'/Volumes/Elisabeth/AKTIV_MPM_tryout/AKTIV1209A/'};
matlabbatch{1}.spm.tools.hmri.create_mpm.subj.sensitivity.RF_us = '-';
%%
matlabbatch{1}.spm.tools.hmri.create_mpm.subj.b1_type.i3D_EPI.b1input = {
                                                                         '/Volumes/Plasticity/EnergI/Preprocessing/MPM/Niftis/AKTIV1209A/B1/sAKTIV1209A_6928-0070-00001-000001-01.nii,1'
                                                                         '/Volumes/Plasticity/EnergI/Preprocessing/MPM/Niftis/AKTIV1209A/B1/sAKTIV1209A_6928-0070-00001-000049-02.nii,1'
                                                                         '/Volumes/Plasticity/EnergI/Preprocessing/MPM/Niftis/AKTIV1209A/B1/sAKTIV1209A_6928-0070-00002-000097-01.nii,1'
                                                                         '/Volumes/Plasticity/EnergI/Preprocessing/MPM/Niftis/AKTIV1209A/B1/sAKTIV1209A_6928-0070-00002-000145-02.nii,1'
                                                                         '/Volumes/Plasticity/EnergI/Preprocessing/MPM/Niftis/AKTIV1209A/B1/sAKTIV1209A_6928-0070-00003-000193-01.nii,1'
                                                                         '/Volumes/Plasticity/EnergI/Preprocessing/MPM/Niftis/AKTIV1209A/B1/sAKTIV1209A_6928-0070-00003-000241-02.nii,1'
                                                                         '/Volumes/Plasticity/EnergI/Preprocessing/MPM/Niftis/AKTIV1209A/B1/sAKTIV1209A_6928-0070-00004-000289-01.nii,1'
                                                                         '/Volumes/Plasticity/EnergI/Preprocessing/MPM/Niftis/AKTIV1209A/B1/sAKTIV1209A_6928-0070-00004-000337-02.nii,1'
                                                                         '/Volumes/Plasticity/EnergI/Preprocessing/MPM/Niftis/AKTIV1209A/B1/sAKTIV1209A_6928-0070-00005-000385-01.nii,1'
                                                                         '/Volumes/Plasticity/EnergI/Preprocessing/MPM/Niftis/AKTIV1209A/B1/sAKTIV1209A_6928-0070-00005-000433-02.nii,1'
                                                                         '/Volumes/Plasticity/EnergI/Preprocessing/MPM/Niftis/AKTIV1209A/B1/sAKTIV1209A_6928-0070-00006-000481-01.nii,1'
                                                                         '/Volumes/Plasticity/EnergI/Preprocessing/MPM/Niftis/AKTIV1209A/B1/sAKTIV1209A_6928-0070-00006-000529-02.nii,1'
                                                                         '/Volumes/Plasticity/EnergI/Preprocessing/MPM/Niftis/AKTIV1209A/B1/sAKTIV1209A_6928-0070-00007-000577-01.nii,1'
                                                                         '/Volumes/Plasticity/EnergI/Preprocessing/MPM/Niftis/AKTIV1209A/B1/sAKTIV1209A_6928-0070-00007-000625-02.nii,1'
                                                                         '/Volumes/Plasticity/EnergI/Preprocessing/MPM/Niftis/AKTIV1209A/B1/sAKTIV1209A_6928-0070-00008-000673-01.nii,1'
                                                                         '/Volumes/Plasticity/EnergI/Preprocessing/MPM/Niftis/AKTIV1209A/B1/sAKTIV1209A_6928-0070-00008-000721-02.nii,1'
                                                                         '/Volumes/Plasticity/EnergI/Preprocessing/MPM/Niftis/AKTIV1209A/B1/sAKTIV1209A_6928-0070-00009-000769-01.nii,1'
                                                                         '/Volumes/Plasticity/EnergI/Preprocessing/MPM/Niftis/AKTIV1209A/B1/sAKTIV1209A_6928-0070-00009-000817-02.nii,1'
                                                                         '/Volumes/Plasticity/EnergI/Preprocessing/MPM/Niftis/AKTIV1209A/B1/sAKTIV1209A_6928-0070-00010-000865-01.nii,1'
                                                                         '/Volumes/Plasticity/EnergI/Preprocessing/MPM/Niftis/AKTIV1209A/B1/sAKTIV1209A_6928-0070-00010-000913-02.nii,1'
                                                                         '/Volumes/Plasticity/EnergI/Preprocessing/MPM/Niftis/AKTIV1209A/B1/sAKTIV1209A_6928-0070-00011-000961-01.nii,1'
                                                                         '/Volumes/Plasticity/EnergI/Preprocessing/MPM/Niftis/AKTIV1209A/B1/sAKTIV1209A_6928-0070-00011-001009-02.nii,1'
                                                                         };
%%
matlabbatch{1}.spm.tools.hmri.create_mpm.subj.b1_type.i3D_EPI.b0input = {
                                                                         '/Volumes/Plasticity/EnergI/Preprocessing/MPM/Niftis/AKTIV1209A/B0/magnitude/sAKTIV1209A_6928-0068-00001-000001-01.nii,1'
                                                                         '/Volumes/Plasticity/EnergI/Preprocessing/MPM/Niftis/AKTIV1209A/B0/magnitude/sAKTIV1209A_6928-0068-00001-000001-02.nii,1'
                                                                         '/Volumes/Plasticity/EnergI/Preprocessing/MPM/Niftis/AKTIV1209A/B0/phase/sAKTIV1209A_6928-0069-00001-000001-02.nii,1'
                                                                         };
matlabbatch{1}.spm.tools.hmri.create_mpm.subj.b1_type.i3D_EPI.b1parameters.b1metadata = 'yes';
matlabbatch{1}.spm.tools.hmri.create_mpm.subj.raw_mpm.MT = {
                                                            '/Volumes/Plasticity/EnergI/Preprocessing/MPM/Niftis/AKTIV1209A/MT/sAKTIV1209A_6928-0071-00001-000176-01.nii,1'
                                                            '/Volumes/Plasticity/EnergI/Preprocessing/MPM/Niftis/AKTIV1209A/MT/sAKTIV1209A_6928-0071-00001-000352-02.nii,1'
                                                            '/Volumes/Plasticity/EnergI/Preprocessing/MPM/Niftis/AKTIV1209A/MT/sAKTIV1209A_6928-0071-00001-000528-03.nii,1'
                                                            '/Volumes/Plasticity/EnergI/Preprocessing/MPM/Niftis/AKTIV1209A/MT/sAKTIV1209A_6928-0071-00001-000704-04.nii,1'
                                                            '/Volumes/Plasticity/EnergI/Preprocessing/MPM/Niftis/AKTIV1209A/MT/sAKTIV1209A_6928-0071-00001-000880-05.nii,1'
                                                            '/Volumes/Plasticity/EnergI/Preprocessing/MPM/Niftis/AKTIV1209A/MT/sAKTIV1209A_6928-0071-00001-001056-06.nii,1'
                                                            };
matlabbatch{1}.spm.tools.hmri.create_mpm.subj.raw_mpm.PD = {
                                                            '/Volumes/Plasticity/EnergI/Preprocessing/MPM/Niftis/AKTIV1209A/PD/sAKTIV1209A_6928-0073-00001-000176-01.nii,1'
                                                            '/Volumes/Plasticity/EnergI/Preprocessing/MPM/Niftis/AKTIV1209A/PD/sAKTIV1209A_6928-0073-00001-000352-02.nii,1'
                                                            '/Volumes/Plasticity/EnergI/Preprocessing/MPM/Niftis/AKTIV1209A/PD/sAKTIV1209A_6928-0073-00001-000528-03.nii,1'
                                                            '/Volumes/Plasticity/EnergI/Preprocessing/MPM/Niftis/AKTIV1209A/PD/sAKTIV1209A_6928-0073-00001-000704-04.nii,1'
                                                            '/Volumes/Plasticity/EnergI/Preprocessing/MPM/Niftis/AKTIV1209A/PD/sAKTIV1209A_6928-0073-00001-000880-05.nii,1'
                                                            '/Volumes/Plasticity/EnergI/Preprocessing/MPM/Niftis/AKTIV1209A/PD/sAKTIV1209A_6928-0073-00001-001056-06.nii,1'
                                                            '/Volumes/Plasticity/EnergI/Preprocessing/MPM/Niftis/AKTIV1209A/PD/sAKTIV1209A_6928-0073-00001-001232-07.nii,1'
                                                            '/Volumes/Plasticity/EnergI/Preprocessing/MPM/Niftis/AKTIV1209A/PD/sAKTIV1209A_6928-0073-00001-001408-08.nii,1'
                                                            };
matlabbatch{1}.spm.tools.hmri.create_mpm.subj.raw_mpm.T1 = {
                                                            '/Volumes/Plasticity/EnergI/Preprocessing/MPM/Niftis/AKTIV1209A/T1/sAKTIV1209A_6928-0075-00001-000176-01.nii,1'
                                                            '/Volumes/Plasticity/EnergI/Preprocessing/MPM/Niftis/AKTIV1209A/T1/sAKTIV1209A_6928-0075-00001-000352-02.nii,1'
                                                            '/Volumes/Plasticity/EnergI/Preprocessing/MPM/Niftis/AKTIV1209A/T1/sAKTIV1209A_6928-0075-00001-000528-03.nii,1'
                                                            '/Volumes/Plasticity/EnergI/Preprocessing/MPM/Niftis/AKTIV1209A/T1/sAKTIV1209A_6928-0075-00001-000704-04.nii,1'
                                                            '/Volumes/Plasticity/EnergI/Preprocessing/MPM/Niftis/AKTIV1209A/T1/sAKTIV1209A_6928-0075-00001-000880-05.nii,1'
                                                            '/Volumes/Plasticity/EnergI/Preprocessing/MPM/Niftis/AKTIV1209A/T1/sAKTIV1209A_6928-0075-00001-001056-06.nii,1'
                                                            };
matlabbatch{1}.spm.tools.hmri.create_mpm.subj.popup = false;
