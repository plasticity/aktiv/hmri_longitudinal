addpath('lib')
addpath(fullfile('lib', 'bids'))

setup_paths
warning('off', 'MATLAB:MKDIR:DirectoryExists');
log_file = start_log(fullfile(deriv_path, 'logs'), mfilename);
spm('defaults','FMRI');

subjects = get_subjects(nifti_path, 'full');

% move the templates to the shoot folder (they end up in the first
% subject folder by default)
system(sprintf('mv %s %s', ...
    [deriv_path '/MPM_noreorient/' cell2mat(subjects(1)) '/Template_?.nii'], ...
    [deriv_path '/MPM_noreorient/shoot']));
clear map1 map2 map3 map4 def final_temp files_temp files_map files_def
for i=1:length(subjects)
    subject = cell2mat(subjects(i));
    sessions = get_sessions(nifti_path, subject);
    
    for j=1:length(sessions)
        session = cell2mat(sessions(j));
        if isfile(spm_select('FPList', fullfile(deriv_path, 'MPM_noreorient', subject, session, 'qMRImaps', 'Results'), '^rws.*\MTsat\.nii$'))
            fprintf('Normalisation has already run, skipping %s %s\n', subject, session);
        else
            tmpid = get_tmpid_s();
            in_dir = fullfile(deriv_path, 'MPM_noreorient', subject, session);
            out_dir = fullfile(deriv_path, 'MPM_noreorient', 'mni');
            mkdir(fullfile(out_dir, 'batches'));
            mkdir(fullfile(out_dir, 'logs'));
            batchfile = fullfile(out_dir, 'batches', ['shoot2-%j-', tmpid, '.mat']);
            batchlog = fullfile(out_dir, 'logs', ['shoot2-%j-', tmpid, '.log']);
            batchcmd = sprintf('%s %s batch %s', compiled_hmri, mcr_path, batchfile);
            
            if isfile(spm_select('FPList', in_dir, '^y_combined_transform.*\.nii$'))
                % AKTIV SETUP WITH 3 TIMEPOINTS
                map1(i*3-3+j,:) = spm_select('FPList', fullfile(in_dir, 'qMRImaps', 'Results'), '^s.*\MTsat\.nii$');
                map2(i*3-3+j,:) = spm_select('FPList', fullfile(in_dir, 'qMRImaps', 'Results'), '^s.*\PD\.nii$');
                map3(i*3-3+j,:) = spm_select('FPList', fullfile(in_dir, 'qMRImaps', 'Results'), '^s.*\R1\.nii$');
                map4(i*3-3+j,:) = spm_select('FPList', fullfile(in_dir, 'qMRImaps', 'Results'), '^s.*\R2s_OLS\.nii$');
                
                def(i*3-3+j,:) = spm_select('FPList', in_dir, '^y_combined_transform.*\.nii$');

                % REPRODUCABILITY SETUP WITH 4 TIMEPOINTS
%                 map1(i*4-4+j,:) = spm_select('FPList', fullfile(in_dir, 'qMRImaps', 'Results'), '^s.*\MTsat\.nii$');
%                 map2(i*4-4+j,:) = spm_select('FPList', fullfile(in_dir, 'qMRImaps', 'Results'), '^s.*\PD\.nii$');
%                 map3(i*4-4+j,:) = spm_select('FPList', fullfile(in_dir, 'qMRImaps', 'Results'), '^s.*\R1\.nii$');
%                 map4(i*4-4+j,:) = spm_select('FPList', fullfile(in_dir, 'qMRImaps', 'Results'), '^s.*\R2s_OLS\.nii$');
%                 
%                 def(i*4-4+j,:) = spm_select('FPList', in_dir, '^y_combined_transform.*\.nii$');
                
            else
                fprintf('Timepoint %s not available for subject %s\n', session, subject);
            end
        end
    end % session
end % subject

final_temp = spm_select('FPList', fullfile(deriv_path, 'MPM_noreorient', 'shoot'), '^Template_4\.nii$');

files_temp = cellstr(final_temp);
files_map = { cellstr(map1), cellstr(map2), cellstr(map3), cellstr(map4) };
files_def = cellstr(def);

% remove empty rows if subject or time point not available
files_map{1}=files_map{1}(~cellfun('isempty',files_map{1}));
files_map{2}=files_map{2}(~cellfun('isempty',files_map{2}));
files_map{3}=files_map{3}(~cellfun('isempty',files_map{3}));
files_map{4}=files_map{4}(~cellfun('isempty',files_map{4}));

files_def=files_def(~cellfun('isempty',files_def));


fprintf('Creating batch for shoot normalize ...\n');
clear matlabbatch
matlabbatch{1}.spm.tools.shoot.norm.template = files_temp;
matlabbatch{1}.spm.tools.shoot.norm.data.subjs.deformations = files_def;
matlabbatch{1}.spm.tools.shoot.norm.data.subjs.images = files_map;
matlabbatch{1}.spm.tools.shoot.norm.vox = [NaN NaN NaN];
matlabbatch{1}.spm.tools.shoot.norm.bb = [NaN NaN NaN
    NaN NaN NaN];
matlabbatch{1}.spm.tools.shoot.norm.preserve = 0;
matlabbatch{1}.spm.tools.shoot.norm.fwhm = 0;

save(batchfile, 'matlabbatch');

fprintf('Submitting\n');
system(sprintf('sbatch -D. -c 1 -J %s -o %s --wrap "%s"', ...
    'shoot2', batchlog, batchcmd));

stop_log(log_file);
