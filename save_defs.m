addpath('lib')
addpath(fullfile('lib', 'bids'))

setup_paths
warning('off', 'MATLAB:MKDIR:DirectoryExists');
log_file = start_log(fullfile(deriv_path, 'logs'), mfilename);
spm('defaults','FMRI');

subjects = get_subjects(nifti_path, 'full');
for i=1:length(subjects)
    subject = cell2mat(subjects(i));
    sessions = get_sessions(nifti_path, subject);
    for j=1:length(sessions)
        session = cell2mat(sessions(j));
        if isfile(spm_select('FPList', fullfile(deriv_path, 'MPM_noreorient', subject, session), '^y_combined_transform.*\.nii$'))
            fprintf('Combined transformations already exist, skipping %s %s\n', subject, session);
        elseif exist(fullfile(deriv_path, 'MPM_noreorient', subject, session, 'qMRImaps', 'Results', 'B1mapCalc'), 'dir')
            fprintf('Map creation unsuccessful, skipping %s %s\n', subject, session);
        elseif ~isfile(spm_select('FPList', fullfile(deriv_path, 'MPM_noreorient', subject), '^y_rc1.*\MTsat_Template\.nii$'))
            fprintf('No transformation to group template available, skipping %s\n', subject);
        else
            tmpid = get_tmpid_s();
            in_dir = fullfile(deriv_path, 'MPM_noreorient', subject, session);
            out_dir = fullfile(deriv_path, 'MPM_noreorient', subject);
            mkdir(fullfile(out_dir, 'batches'));
            mkdir(fullfile(out_dir, 'logs'));
            batchfile = fullfile(out_dir, 'batches', [subject, '_', session, '_save_def-', tmpid, '.mat']);
            batchlog = fullfile(out_dir, 'logs', [subject, '_', session, '_save_def-%j-', tmpid, '.log']);
            batchcmd = sprintf('%s %s batch %s', compiled_hmri, mcr_path, batchfile);
            
            clear def_intra def_inter
            def_intra = spm_select('FPList', fullfile(in_dir, 'qMRImaps', 'Results'), '^y_rc1.*\MTsat_clean_Template\.nii$');
            def_intra = cellstr(def_intra);
            
            def_inter = spm_select('FPList', out_dir, '^y_rc1.*\MTsat_Template\.nii$');
            def_inter = cellstr(def_inter);
            
            fprintf('Creating batch for %s/%s ...\n', subject, session);
            clear matlabbatch
            matlabbatch{1}.spm.util.defs.comp{1}.def = def_intra;
            matlabbatch{1}.spm.util.defs.comp{2}.def = def_inter;
            matlabbatch{1}.spm.util.defs.out{1}.savedef.ofname = ['combined_transform' subject session];
            matlabbatch{1}.spm.util.defs.out{1}.savedef.savedir.saveusr = {in_dir};
            
            save(batchfile, 'matlabbatch');
            
            fprintf('Submitting\n');
            system(sprintf('sbatch -D. -c 1 -J %s -o %s --wrap "%s"', ...
                ['save_def_', subject, '_', session], ...
                batchlog, batchcmd));
        end
    end % session
end % subject
stop_log(log_file);
