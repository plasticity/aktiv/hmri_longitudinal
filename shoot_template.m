addpath('lib')
addpath(fullfile('lib', 'bids'))

setup_paths
warning('off', 'MATLAB:MKDIR:DirectoryExists');
log_file = start_log(fullfile(deriv_path, 'logs'), mfilename);
spm('defaults','FMRI');

if exist(fullfile(deriv_path, 'MPM_noreorient', 'shoot'))
    fprintf('Dartel has already run, skipping\n');
else
    subjects = get_subjects(nifti_path, 'full');
    clear c1_image files
    for i=1:length(subjects)
        subject = cell2mat(subjects(i));
        sessions = get_sessions(nifti_path, subject);
        if isfile(spm_select('FPList', fullfile(deriv_path, 'MPM_noreorient', subject), '^rc1median_.*\MTsat\.nii$'))

            tmpid = get_tmpid_s();
            in_dir = fullfile(deriv_path, 'MPM_noreorient', subject);
            outdir = fullfile(deriv_path, 'MPM_noreorient', 'shoot');
            mkdir(outdir);
            mkdir(fullfile(outdir, 'batches'));
            mkdir(fullfile(outdir, 'logs'));
            batchfile = fullfile(outdir, 'batches', ['shoot1-', tmpid, '.mat']);
            batchlog = fullfile(outdir, 'logs', ['shoot1-%j-', tmpid, '.log']);
            batchcmd = sprintf(['. /etc/profile; module unload matlab/R2017b; module load matlab/R2017b; matlab -r \\"' ...
                'addpath(''%s''); ' ...
                'addpath(genpath(''%s'')); ' ...
                'spm(''defaults'', ''fmri''); ' ...
                'spm_jobman(''initcfg''); ' ...
                'load(''%s''); ' ...
                'spm_jobman(''run'', matlabbatch); ' ...
                'quit;\\"'], ...
                spm_with_hmri, ...
                fullfile(spm_with_hmri, 'toolbox', 'hMRI'), ...
                batchfile);
            
            c1_image(i,:)  = spm_select('FPList', in_dir, '^rc1median_.*\MTsat\.nii$');
            c2_image(i,:)  = spm_select('FPList', in_dir, '^rc2median_.*\MTsat\.nii$');
        else
            fprintf('No rc1 average for %s, skipping\n', subject);
        end
    end
    files = {cellstr(c1_image), cellstr(c2_image)};
    % remove empty cells in case subjects have no longitudinal data
    files=files(~cellfun('isempty',files));
    
    fprintf('Creating batch for shoot ...\n');
    clear matlabbatch
    matlabbatch{1}.spm.tools.shoot.warp.images = files;
    
    save(batchfile, 'matlabbatch');
    
    fprintf('Submitting\n');
    system(sprintf('sbatch -D. -c 1 -J %s -o %s -p long --time 120:0:0 --wrap "%s"', ...
        'shoot1', batchlog, batchcmd));
end

stop_log(log_file);

