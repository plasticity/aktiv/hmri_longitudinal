addpath('lib')
addpath(fullfile('lib', 'bids'))

setup_paths
warning('off', 'MATLAB:MKDIR:DirectoryExists');
log_file = start_log(fullfile(deriv_path, 'logs'), mfilename);
spm('defaults','FMRI');

subjects =  get_subjects(nifti_path, 'full');
for i=1:length(subjects)
    subject = cell2mat(subjects(i));
    if isfile(spm_select('FPList', fullfile(deriv_path, 'MPM_noreorient', subject), '^c1median_.*\MTsat\.nii$'))
        fprintf('Segmentation has already run, skipping %s\n', subject);
    elseif ~isfile(spm_select('FPList', fullfile(deriv_path, 'MPM_noreorient', subject), '^median.*MTsat\.nii$'))
        fprintf('No median image available, skipping %s\n', subject);
    else
        tmpid = get_tmpid_s();
        in_dir = fullfile(deriv_path, 'MPM_noreorient', subject);
        out_dir = in_dir;
        mkdir(fullfile(out_dir, 'batches'));
        mkdir(fullfile(out_dir, 'logs'));
        batchfile = fullfile(out_dir, 'batches', [subject, '_segment-', tmpid, '.mat']);
        batchlog = fullfile(out_dir, 'logs', [subject, '_segment-%j-', tmpid, '.log']);
        batchcmd = sprintf('%s %s batch %s', compiled_hmri, mcr_path, batchfile);

        clear file_MT file_PD
        file_MT = spm_select('FPList', in_dir, '^median.*MTsat\.nii$');
        file_PD = spm_select('FPList', in_dir, '^median.*PD\.nii$');
        
        fprintf('Creating batch for %s ...\n', subject);
        clear matlabbatch
        matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.many_sdatas.output.indir = 1;
        matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.many_sdatas.channel(1).vols = {file_MT};
        matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.many_sdatas.channel(1).biasreg = 1;
        matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.many_sdatas.channel(1).biasfwhm = Inf;
        matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.many_sdatas.channel(1).write = [0 0];
        matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.many_sdatas.channel(2).vols = {file_PD};
        matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.many_sdatas.channel(2).biasreg = 0.001;
        matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.many_sdatas.channel(2).biasfwhm = Inf;
        matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.many_sdatas.channel(2).write = [0 0];
        matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.many_sdatas.vols_pm = {};
        matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.many_sdatas.vox = [1 1 1];
        matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.many_sdatas.bb = [-78 -112 -70
            78 76 85];
        matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.tissue(1).tpm = {fullfile(spm_with_hmri, 'toolbox', 'hMRI' , 'etpm', 'eTPM.nii,1')};
        matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.tissue(1).ngaus = 2;
        matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.tissue(1).native = [1 1];
        matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.tissue(1).warped = [0 0];
        matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.tissue(2).tpm = {fullfile(spm_with_hmri, 'toolbox', 'hMRI' , 'etpm', 'eTPM.nii,2')};
        matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.tissue(2).ngaus = 2;
        matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.tissue(2).native = [1 1];
        matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.tissue(2).warped = [0 0];
        matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.tissue(3).tpm = {fullfile(spm_with_hmri, 'toolbox', 'hMRI' , 'etpm', 'eTPM.nii,3')};
        matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.tissue(3).ngaus = 2;
        matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.tissue(3).native = [1 0];
        matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.tissue(3).warped = [0 0];
        matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.tissue(4).tpm = {fullfile(spm_with_hmri, 'toolbox', 'hMRI' , 'etpm', 'eTPM.nii,4')};
        matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.tissue(4).ngaus = 3;
        matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.tissue(4).native = [0 0];
        matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.tissue(4).warped = [0 0];
        matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.tissue(5).tpm = {fullfile(spm_with_hmri, 'toolbox', 'hMRI' , 'etpm', 'eTPM.nii,5')};
        matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.tissue(5).ngaus = 4;
        matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.tissue(5).native = [0 0];
        matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.tissue(5).warped = [0 0];
        matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.tissue(6).tpm = {fullfile(spm_with_hmri, 'toolbox', 'hMRI' , 'etpm', 'eTPM.nii,6')};
        matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.tissue(6).ngaus = 2;
        matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.tissue(6).native = [0 0];
        matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.tissue(6).warped = [0 0];
        matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.warp.mrf = 1;
        matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.warp.cleanup = 1;
        matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.warp.reg = [0 0.001 0.5 0.05 0.2];
        matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.warp.affreg = 'mni';
        matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.warp.fwhm = 0;
        matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.warp.samp = 3;
        matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.warp.write = [0 1];
        matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.warp.vox = NaN;
        matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.warp.bb = [NaN NaN NaN
            NaN NaN NaN];
        
        save(batchfile, 'matlabbatch');
        
        fprintf('Submitting\n');
        system(sprintf('sbatch -D. -c 1 -J %s -o %s --wrap "%s"', ...
            ['segment_', subject], ...
            batchlog, batchcmd));
    end %if
end % subject
stop_log(log_file);
