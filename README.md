## Summary

This is a longitudinal version of the hMRI pipeline.
There are similar, individual stages. They must be individually started **and
waited for**! Each stage will create batch and save files and logs. The first 2 steps are included for reasons of reproducability 
but they simply convert the DICOM files to the following adapted BIDS format:

![Required folder structure](doc/folder_structure.png)

From step 3 (createmaps) onwards, the scripts should work (with adapted paths)
on datasets following the same structure. 

The batches for each stage are created and submitted with:

#### 0. Prerequisite

Edit the user specific section of [setup_paths.m](setup_paths.m) to set input/output folders and point to your SPM12 (with
hMRI, see Build Section).

#### 1. import Dicom files
**dcms2nifti.m**
```
module load matlab/R2017b # if not done already
matlab -r 'dcms2nifti; quit'
```

#### 2. merge duplicate `_1` sessions (some participants had some sequences measured twice due to interruptions, these are correctly merged here)
**merge_data.m**:
```
module load matlab/R2017b # if not done already
matlab -r 'merge_data; quit'
```

#### 3. create maps (auto-reorient is part of the original hMRI pipeline but in our case seems to rather harm than benefit the data, therefore it's left out)
**createmaps_noreorient.m**:
```
module load matlab/R2017b # if not done already
matlab -r 'createmaps_noreorient; quit'
```

#### 4. threshold maps (segmentation performance is much improved on the thresholded maps)
**threshold_maps.m**:
```
module load matlab/R2017b # if not done already
matlab -r 'threshold_maps; quit'
```

#### 5. segment MT and PD maps with multichannel segmentation (shoot toolbox only works with images that have isotropic voxels, identical dimensions and which are in approximate alignment with each other. Therefore, shoot registration in the next step must be based on images that are first “imported” via the Segment toolbox)
**segmaps_multichannel.m**:
```
module load matlab/R2017b # if not done already
matlab -r 'segmaps_multichannel; quit'
```

#### 6. create within-subject template (run the diffeomorphic registration tool with geodesic shooting “SHOOT” to create a within-subject template across sessions)
**shoot_subjecttemplate.m**:
```
module load matlab/R2017b # if not done already
matlab -r 'shoot_subjecttemplate; quit'
```

#### 7. apply deformations (apply the deformations to the raw MT and PD maps to bring them into the same, i.e. witin-subject template space)
**apply_shootdefs.m**:
```
module load matlab/R2017b # if not done already
matlab -r 'apply_shootdefs; quit'
```

#### 8. compute MT and PD median maps (compute the median MT and PD maps across timepoints)
**comp_medians.m**:
```
module load matlab/R2017b # if not done already
matlab -r 'comp_medians; quit'
```

#### 9. segment MT and PD median maps (segment the median images again because the shoot toolbox only works with images that have isotropic voxels, identical dimensions and which are in approximate alignment with each other. Therefore, shoot registration in the next step must be based on images that are first “imported” via the Segment toolbox)
**segmedian_multichannel.m**:
```
module load matlab/R2017b # if not done already
matlab -r 'segmedian_multichannel; quit'
```

#### 10. create group template (run the diffeomorphic registration tool with geodesic shooting “SHOOT” to create a between-subject or group template across participants)
**shoot_template.m**:
```
module load matlab/R2017b # if not done already
matlab -r 'shoot_template; quit'
```

#### 11. combine and save the within and between-subject deformations (combine and save within + between deformations)
**save_defs.m**:
```
module load matlab/R2017b # if not done already
matlab -r 'save_defs; quit'
```

#### 12. normalize group template to MNI using shoot 
**shoot_normalize.m**:
```
module load matlab/R2017b # if not done already
matlab -r 'shoot_normalize; quit'
```

#### 13. normalize median gray and white matter segments **with modulation** to MNI for the weighted smoothing afterwards
**shoot_modulated_normalize.m**:
```
module load matlab/R2017b # if not done already
matlab -r 'shoot_modulated_normalize; quit'
```

#### 14. apply weighted smoothing to the final maps
**smooth.m**:
```
module load matlab/R2017b # if not done already
matlab -r 'smooth; quit'
```

Each script will not submit a job again, when some known output file already
exists. Check the source for the individual checks.

To start over, delete (some) outputfiles for a specific subject and run a stage again. For instance:

```
rm -rf output/derivatives/MPM/sub-*/ses-*/reorient*
rm -rf output/derivatives/MPM/sub-*/ses-*/qMRI*
```

## Extraction of mean values and standard deviations for predefined ROIs
**see folder extract_from_ROIs**


## Build

**Attention**: Currently it's not possible to run the map processing pipeline
with a compiled batch. This might get fixed at some point however. It's
entirely possibe to run the batches in regular matlab sessions, given there are
enough free licenses.

For all other steps, you should build a compiled spm12+hmri:

1. Download and extract [spm12](https://www.fil.ion.ucl.ac.uk/spm/download/restricted/eldorado/spm12.zip)

Example:
```
wget https://www.fil.ion.ucl.ac.uk/spm/download/restricted/eldorado/spm12.zip
unzip spm12.zip
mkdir -p $HOME/matlab/tools
mv spm12 $HOME/matlab/tools/spm12_hmri
# optionally clean up pre-compiled mex files for other platforms:
cd $HOME/matlab/tools/spm12_hmri
find . -name '*.mexw*' -delete
find . -name '*.mexmaci64' -delete
```

2. Download the hMRI toolbox (either the [original](https://github.com/hMRI-group/hMRI-toolbox/releases) or [our fork](https://github.com/MPIB/hMRI-toolbox) to a folder called `toolbox/hMRI` in an existing spm12 folder called `spm12_hmri/`.

Example:
```
cd $HOME/matlab/tools/spm12_hmri/toolbox
git clone https://github.com/hMRI-group/hMRI-toolbox hMRI
```

3. Fix overrides from hMRI

`spm_...` overrides in `toolbox/hMRI/` **must** be removed from the spm12 base
directory. The compiled archive cannot handle path prioririties properly.
Functions such as `spm_cfg_dicom` are then still taken from spm12 instead of
from hMRI. Keep in mind that this also means, the newly created `spm12_hmri/`
won't be functional without the hMRI toolbox.

```
cd $HOME/matlab/tools/spm12_hmri
for f in $(find toolbox/hMRI/spm12/ -iname 'spm_*'); do r=$(echo $f | cut -d"/" -f4-) ; if [ -f $r ] ; then rm $r ; fi ; done
cp toolbox/hMRI/spm12/spm_jsonread.mexa64 .
```

Build `run_spm12.sh` (with correct paths):

```
addpath('~/matlab/tools/spm12_hmri')
addpath('~/matlab/tools/spm12_hmri/config')
addpath(genpath('~/matlab/tools/spm12_hmri/toolbox/hMRI/'))
spm_make_standalone
```
